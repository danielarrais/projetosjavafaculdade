
public class Principal {

	public static void main(String[] args) throws CloneNotSupportedException {
		ArvoreBinariaBusca arvore = new ArvoreBinariaBusca();

		arvore.adicionar(null, "50");
		arvore.adicionar(arvore.raiz, "25");
		arvore.adicionar(arvore.raiz, "10");
		arvore.adicionar(arvore.raiz, "35");
		arvore.adicionar(arvore.raiz, "80");
		arvore.adicionar(arvore.raiz, "60");
		arvore.adicionar(arvore.raiz, "88");
		
		System.out.println("Arvore: ");
		arvore.pecorreNivel(arvore.raiz);
		System.out.println();
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "50").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "88").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "25").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "60").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "80").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "10").dado);arvore.pecorreNivel(arvore.raiz);
		System.out.println("Removido: "+arvore.remover(arvore.raiz, "35").dado);arvore.pecorreNivel(arvore.raiz);
		
		System.out.println();
		System.out.println("Arvore: ");
		arvore.pecorreNivel(arvore.raiz);
	}

}
