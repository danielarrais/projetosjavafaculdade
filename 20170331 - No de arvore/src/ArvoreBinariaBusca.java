import java.util.ArrayDeque;
import java.util.Deque;

public class ArvoreBinariaBusca {
	No raiz = null;
	No paiDaRaiz = null;

	public void pecorreNivel(No no) {
		if (no == null) {
			System.out.println(null + "");
		} else {
			Deque<No> fila = new ArrayDeque();
			fila.add(no);
			no.nivel = 0;
			while (!fila.isEmpty()) {
				No atual = fila.removeFirst();
				System.out.printf("%s", "N�vel: " + atual.nivel + ": " + atual.dado + "\n");
				if (atual.esquerdo != null)
					fila.add(atual.esquerdo);
				if (atual.direito != null)
					fila.add(atual.direito);
			}
		}
	}

	public void pecorreOrdem(No raiz) {
		if (raiz != null) {
			if (raiz.esquerdo != null) {
				pecorreOrdem(raiz.esquerdo);
			}
			System.out.println(raiz.dado + "   nivel: " + raiz.nivel);
			if (raiz.direito != null) {
				pecorreOrdem(raiz.direito);
			}
		} else {
			System.out.println(null + "");
		}

	}

	public void pecorrePosOrdem(No raiz) {
		if (raiz != null) {
			if (raiz.esquerdo != null) {
				pecorrePosOrdem(raiz.esquerdo);
			}
			if (raiz.direito != null) {
				pecorrePosOrdem(raiz.direito);
			}
			System.out.println(raiz.dado + "   nivel: " + raiz.nivel);
		} else {
			System.out.println(null + "");
		}

	}

	public No buscarNo(No raiz, String dado) {
		if (raiz == null) {
			return raiz;
		} else {
			if (dado.equals(raiz.dado)) {
				return raiz;
			} else if (raiz.dado.compareTo(dado) > 0) {
				paiDaRaiz = raiz;
				return buscarNo(raiz.esquerdo, dado);
			} else if (raiz.dado.compareTo(dado) < 0) {
				paiDaRaiz = raiz;
				return buscarNo(raiz.direito, dado);
			}
		}
		return buscarNo(raiz, dado);
	}

	public No remover(No raiz, String dado) {
		No excluido, excluir = buscarNo(this.raiz, dado);
		No pai = getPai(excluir);
		if (excluir == null) {
			return null;
		} else {
			excluido = new No(excluir.dado);
		}
		if (excluir.esquerdo == null && excluir.direito == null) {
			if (pai != null && (pai.esquerdo != null || excluir.direito != null)) {
				if (pai.esquerdo.dado.equals(excluir.dado) && pai.esquerdo != null) {
					pai.esquerdo = null;
				} else {
					pai.direito = null;
				}
			} else {
				this.raiz = null;
			}
			return excluido;
		} else {
			if (excluir.direito != null && excluir.esquerdo == null) {
				excluir.dado = excluir.direito.dado;
				excluir.direito = null;
				return excluido;
			} else if (excluir.direito == null && excluir.esquerdo != null) {
				excluir.dado = excluir.esquerdo.dado;
				excluir.esquerdo = null;
				return excluido;
			} else {
				No copia = noMaior(excluir.esquerdo);
				String maior = copia.dado;
				remover(raiz, copia.dado);
				excluir.dado = maior;
				return excluido;
			}
		}
	}

	public No getPai(No no) {
		if (no != null) {
			no = buscarNo(raiz, no.dado);
			no = this.paiDaRaiz;
		}
		return no;
	}

	public No noMaior(No raiz) {
		if (raiz.direito == null) {
			return raiz;
		}
		return noMaior(raiz.direito);
	}

	public void pecorrePreOrdem(No raiz) {
		if (raiz != null) {
			System.out.println(raiz.dado + "nivel: " + raiz.nivel);
			if (raiz.esquerdo != null) {
				pecorrePosOrdem(raiz.esquerdo);
			}
			if (raiz.direito != null) {
				pecorrePosOrdem(raiz.direito);
			}
		}
	}

	public void adicionar(No raiz, String dado) {
		if (this.raiz == null) {
			this.raiz = new No(dado);
			return;
		}
		if (raiz.dado.compareTo(dado) < 0) {
			if (raiz.direito == null) {
				raiz.direito = new No(dado);
				raiz.direito.nivel = raiz.nivel + 1;
				raiz.direito.nivelInverso = raiz.nivel - 1;
			} else {
				adicionar(raiz.direito, dado);
			}
		} else if (raiz.dado.compareTo(dado) > 0) {
			if (raiz.esquerdo == null) {
				raiz.esquerdo = new No(dado);
				raiz.esquerdo.nivel = raiz.nivel + 1;
				raiz.esquerdo.nivelInverso = raiz.nivel - 1;
			} else {
				adicionar(raiz.esquerdo, dado);
			}
		}
	}
}
