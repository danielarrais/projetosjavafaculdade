
public class No implements Cloneable{
	public String dado;
	public No esquerdo;
	public No direito;
	public int nivel;
	public int nivelInverso;
	
	public No(String conteudo)
	{
		dado = conteudo;
		nivel =0;
		direito = null;
		esquerdo = null;
	}

	public No(String dado, No esquerdo, No direito) {
		super();
		this.dado = dado;
		this.esquerdo = esquerdo;
		this.direito = direito;
	}
	
}