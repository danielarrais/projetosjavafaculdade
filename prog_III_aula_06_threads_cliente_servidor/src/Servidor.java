

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Servidor extends JFrame implements Runnable, ActionListener{
	JTextArea areaTexto = null;
	JTextField campoTexto = null;
	JButton botaoOk = null;
	ServerSocket servidor = null;
	Socket cliente = null;
	DataInputStream canalEntrada = null;
	DataOutputStream canalSaida = null;
	
	public Servidor() throws HeadlessException{
		setSize(300,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("servidor Aguardadno...");
		setLayout(new BorderLayout());
		
		campoTexto = new JTextField(20);
		botaoOk = new JButton("Ok");
		areaTexto = new JTextArea(10,10);
		
		try {
			servidor = new ServerSocket(8081);
			cliente = servidor.accept();
			canalEntrada = new DataInputStream(cliente.getInputStream());
			setTitle("Nova conex�o com o cliente " +
	                 cliente.getInetAddress().getHostAddress());
			canalSaida = new DataOutputStream(cliente.getOutputStream());
			new Thread(this).start();
		} catch (IOException e) {
		}
		
		JScrollPane scroll = new JScrollPane(areaTexto);
		
		add(scroll, BorderLayout.CENTER);
		
		JPanel painel = new JPanel();
		painel.setLayout(new FlowLayout());
		painel.add(campoTexto);
		botaoOk.addActionListener(this);
		painel.add(botaoOk);
		
		
		add(painel, BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	public static void main(String args[]){
		new Servidor();
	}

	@Override
	public void run() {
		while (true) {
			try {
				String msg = canalEntrada.readUTF();
				areaTexto.append(msg+"\n");
			} catch (IOException e) {
			}
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			canalSaida.writeUTF(campoTexto.getText());
		} catch (Exception e2) {
			// TODO: handle exception
		}
	}
	
}
