package Mecanica.Interfaces;

import RecursosExternos.BancoDePalavras;

public interface MecanicaDoJogo {

    
    //get's
    public String getDica();
    public boolean isError();
    public boolean isTemDica();
    public int getDicasAcertos();
    public int getAcertos();
    public int getErros();
    public float getPontos();
    public String getPalavra();
    public String getInfor();
    public BancoDePalavras getBancoDePalavras();
    public String getPalavraEmbalharada();
     public float getValorAcerto();
//set's
    public void setBancoDePalavras(String Arquivo);
    public void setError(boolean Error);
    public void setAcertos();
    public void setErros();
    public void setPalavraEmbalharada();
    public void setAcertouDica();
    //Outros métodos
    public void ativarDicas(boolean usouDica);
    public void novaPalavra();
    public boolean perdeu();
    public boolean jogar(String Resposta);
    public void reiniciarJogo();

}
