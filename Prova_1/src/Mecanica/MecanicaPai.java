package Mecanica;

import Mecanica.Interfaces.MecanicaDoJogo;
import Embaralhador.Interfaces.Embaralhador;
import Embaralhador.*;
import RecursosExternos.*;
//Classe abstrata que deverá ser usada para criar as mecanicas
//que por herdarem de uma implementação de MecanicaDoJogo, também serão
//implementações de MecanicaDoJogo

public abstract class MecanicaPai implements MecanicaDoJogo {

    //Instanciação da classe responsável por retornar uma palavra qualquer do Banco de Palavras
    private BancoDePalavras banco = new BancoDePalavras("verbos.txt");
    //Objeto da interface responsável por embaralhar a apalavra retornada por BancoDePalavras
    private Embaralhador embaralhador = FabricaEmbaralhador.retorno();
    //Strings que armazenam a palavra retornada de BancoDePalavras, e Embaralhador
    private String palavra, pEmbalharada;
    //Variável boolean usada para armazenar true para erro e false para acerto
    //temDica vai ser reposnsavel por ditar se o jogo tem dicas disponiveis
    private boolean Error, temDica;
    //Campos para contagem de acertos, erros e nível do usuário jogador
    private int acertos = 0, erros = 0, dicasAcertos = 0, nivel = 3;
    final float pontosPorAcertos = 13;
    //Metodo que retornará true caso as dicas estejam ativadas, e false caso contrário
    public boolean isTemDica() {
        return temDica;
    }

    public float getValorAcerto() {
        return pontosPorAcertos;
    }

    //O metodo isError retorna o valor retornado no ultimo uso do metodo jogar()
    public boolean isError() {
        return Error;
    }

    //Esse metodo retorna a quantidade de acerto de repostas de dicas
    public int getDicasAcertos() {
        return dicasAcertos;
    }

    //Esse metodo acrescenta um a cada dica acertada
    //Para evitar erros ele verifica se o usuário acertou mesmo
    public void setAcertouDica() {
        if (!isError()) {
            this.dicasAcertos++;
        }
    }

    //Esse metodo retorna a quantidade de acertos do jogador
    public int getAcertos() {
        return acertos;
    }

    //Esse metodo é usado para retorna uma dica ao usuario
    public String getDica() {
        //parte da palavra em jogo é armazenda em String Parte
        String parte = getPalavra().substring(0, (int) getPalavra().length() / 2);
        String ocult = "";
        //O resto da palavra e substituida por "*"
        for (int i = 0; i < Math.abs((getPalavra().length() / 2) - getPalavra().length()); i++) {
            ocult = ocult + "*";
                    
        }
        //O metodo retorna parte da palavra
        return parte + ocult;
    }

    //Metodo usado para retornar a quantidade de pontos do usuário
    public float getPontos() {
        //para evitar erros,como pontuação negativa é verificada se o usuário 
        //possui alguma acerto referente a dicas, e acertos gerais.
        //Acerto referente a dicas valem apenas metade da pontuação normal
        return (getAcertos() > 0 && getDicasAcertos() > 0
                ? (getValorAcerto() * (getAcertos() - getDicasAcertos())) + (getValorAcerto() * getDicasAcertos()) / 2
                : getValorAcerto() * getAcertos());
    }

    //Metodo que retornará em formato de String informações detalhadas
    //acerca da mecanica usada no jogo
    public abstract String getInfor();

    //metodo que retorna a quantidade de erros acumuladas pelo usuário
    public int getErros() {
        return erros;
    }

    //metodo que retorna a palavra em jogo
    public String getPalavra() {
        return palavra;
    }

    //metodo usado para retornar o banco de palavra
    public BancoDePalavras getBancoDePalavras() {
        return banco;
    }

    @Override
    //Metodo usado para retornar a palavra embaralhada
    public String getPalavraEmbalharada() {
        return pEmbalharada;
    }

    //Metodo usado para armazenar o estado de erro
    public void setError(boolean Error) {
        this.Error = Error;
    }

    //metodo que adiciona 1 toda vez que o usuário acerta uma resposta
    public void setAcertos() {
        this.acertos++;
    }

    //metodo que adiciona 1 toda vez que o usuário erra uma resposta
    public void setErros() {
        this.erros++;
    }

    //O metodo setBancoDePalavras serve para alterar a origem das palavras
    //Para isso basta passa o link do arquivo em formato String
    public void setBancoDePalavras(String Arquivo) {
        this.banco = new BancoDePalavras(Arquivo);
    }

    //O método setPalavraEmbalharada embaralha a palavra existente
    //armazenada em "palavra" e usa o embaralhador para embaralha-la,
    //colocando-a em "pEmbaralhada"
    public void setPalavraEmbalharada() {
        pEmbalharada = embaralhador.embaralha(palavra);
    }
    public void aumetarNivel(){
        if (getAcertos()%2==0 && getAcertos()==getNivel()*2) {
            nivel++;
        }
    }
    public int getNivel(){
        return nivel-2;
    }
    //O metodo novaPalavra pega uma nova palavra do banco, e a embaralha
    public void novaPalavra() {
        aumetarNivel();
        if (nivel<=16) {
            do {
                this.palavra = banco.sorteiaPalavra(nivel);
        } while (palavra.equalsIgnoreCase("exit"));
        setPalavraEmbalharada();
        }else{
            do {
                this.palavra = banco.sorteiaPalavra();
        } while (palavra.equalsIgnoreCase("exit"));
        setPalavraEmbalharada();
        }
        
    }
    //Metodo usado para ativar dicas no jogo
    //true ativa, e false desativa

    public void ativarDicas(boolean temDica) {
        this.temDica = temDica;
    }

    //Construtor da classe, ao ser instanciada já armazena uma palavra
    //através do método novaPalavra
    public MecanicaPai() {
        novaPalavra();
    }

    //Metodo responsavel por verificar o erro do jogador
    //a resposta deve ser passada como argumento
    public boolean jogar(String Resposta) {
        querSair(Resposta);
        //O bloco abaixo verifica se errou, retornando true ou false
        if (Resposta.equalsIgnoreCase(getPalavra())) {
            setError(false);
            return isError();
        } else {
            setError(true);
            return isError();
        }
    }

    //método que retorna true se o usuário perdeu, e false caso contrário
    //ele deve ser implementado nas classes filhas
    public abstract boolean perdeu();

    //metodo que será responsavel por reiciniar o jogo, caso o usuario escolha opte
    //ele zera todas as informações do jogo
    public void reiniciarJogo() {
        acertos = 0;
        erros = 0;
        dicasAcertos = 0;
        Error = false;
    }

    //Encerra o jogo
    public void querSair(String Resposta) {
        if (Resposta.equalsIgnoreCase("exit")) {
            System.out.println("Jogo Encerrado!!!");
            System.exit(0);
        }
    }

    //Metodo toString() que retorna informações sobre o objeto
    public String toString() {
        return "-----------------------------------\n"
                + "Acertos: " + getAcertos()
                + "   Erros: " + getErros()
                + "   Pontos: " + getPontos()
                + "\n" + "-----------------------------------"
                
                + "\n" + "---Nivel: "+getNivel()+"----"
                + "\n" + "-----------------------------------\n";
    }

}
