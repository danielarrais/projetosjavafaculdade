package Mecanica;

public class MecanicaComVidas extends MecanicaPai {

    //Campo estático que armazena a quantidade de vidas do jogo
    public static final int vidas = 3;

    //Metodo que retorna a quantidade de vidas disponiveis ao usuaŕio
    public int getVidas() {
        return vidas;
    }

    //Metodo retorna a quantidade de vidas restantes
    public int vidasRestantes() {
        return getVidas() - getErros();
    }

    //implementação do metodo abstrato getInfor
    public String getInfor() {
        return "1. Cada questão vale: " + getValorAcerto() + " pontos"
                + "\n2. Se usar a dica você só ganha metade da pontuação: " + getValorAcerto() / 2
                + "\n3. Você pode errar até " + vidas + " vezes, que corresponde a quantidade de vidas"
                + "\n4. Digite EXIT no lugar da palavra para encerrar o jogo\n";
    }

    //Implementação do metodo abstrato perdeu
    //Ele retorna true se a quantidade de erros for maior que a de vidas
    //e false caso contrário
    public boolean perdeu() {
        return (getErros() == getVidas() ? true : false);
    }

    //Metodo toString() que retorna informações sobre o objeto
    public String toString() {
        return (!perdeu() ? super.toString() + "-----------VIDAS:" + vidasRestantes() + "-----------------\n" : super.toString() + "--VOCÊ PERDEU TODAS AS SUAS VIDAS--" + "\n");
    }

}
