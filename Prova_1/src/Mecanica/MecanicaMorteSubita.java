package Mecanica;

public class MecanicaMorteSubita extends MecanicaPai {
    //implementação do metodo abstrato getInfor
    public String getInfor() {
        return "1. Cada questão vale: "+getValorAcerto()+" pontos"
                + "\n2. Se usar a dica você só ganha metade da pontuação: "+getValorAcerto()/2
                + "\n3. Você não pode errar, se isso acontecer o Jogo encerra "
                + "\n4. Digite EXIT no lugar da palavra para encerrar o jogo\n";
    }
    
    //Implementação do metodo abstrato perdeu
    //Ele retorna true se a quantidade de erros for igual a 1
    //e false caso contrário
    public boolean perdeu() {
        return (getErros() ==1?true:false);
    }
}
