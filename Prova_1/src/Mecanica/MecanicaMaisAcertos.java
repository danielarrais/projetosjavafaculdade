package Mecanica;

public class MecanicaMaisAcertos extends MecanicaPai {

    //Implementação do método abstrato getInfor
    public String getInfor() {
        return "1. Cada questão vale: " + getValorAcerto() + " pontos"
                + "\n2. Se usar a dica você só ganha metade da pontuação: "+pontosPorAcertos/2+"\n"
                + "\n3. Você não pode errar a primeira resposta"
                + "\n4. Você joga enquanto a quantidade de erro for menor ou igual a de acertos"
                + "\n4. Digite EXIT no lugar da palavra para encerrar o jogo\n";
    }

    //Implementação do metodo abstrato perdeu
    //Ele retorna true se a quantidade de erros for maior que a de acertos,
    //e false caso contrário
    public boolean perdeu() {
        return (getErros() > getAcertos()?true:false);
    }

}
