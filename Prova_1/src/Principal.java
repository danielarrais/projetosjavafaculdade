

import static Mecanica.FabricaMecanica.*;
import Mecanica.Interfaces.MecanicaDoJogo;
import java.util.*;
//Classe contendo o metodo main, que será responsavel  pela interação com o usuario

public class Principal {

    public static void main(String[] args) {
        //Váriavel Scanner para entrada de dados do usuario
        Scanner s = new Scanner(System.in);
        //Váriavel do tipo String para armazenar entrada do usuário
        String Resposta = null;
        int opcao;
        //Boas vinda do jogo
        System.out.println("-----------------------------------");
        System.out.println("-----Bem vindo ao WORD JUMBLED-----");
        System.out.println("-----------------------------------");
        System.out.println("--ESCOLHA UMA OPÇÃO E DIVIRTA-SE---\n");
        //Saída com as opçoes de jogo disponiveis para escolha do usuário
        System.out.println("1 - Morte Subita!");
        System.out.println("2 - Mais acertos!");
        System.out.println("3 - Com vidas!");
        System.out.println("Você deseja jogar usando qual modalidade?");
        //Referência a interface, que irá armazenar a instância retornada pela fabrica mecanica
        MecanicaDoJogo m = null;
        //Referencia recebe a intancia com o modo de jogo escolhiado pelo usuário
        m = retornaMecanica(s.nextInt());
        
        //Enquanto o usuario não informar uma opçao válida, o programa continuará insistindo
        while (m == null ) {
            System.out.println("Informe uma opção valida!!!:");
            m = retornaMecanica(s.nextInt());
        }
        //Aqui é impresso informações detalhadas acerca do modo de 
        //jogo escolhido usando o método getInfor()
        System.out.println("\nREGRAS:");
        System.out.println(m.getInfor());
        //Aqui o programa pergunta ao usuário se ele irá querer dicas durante o jogo
        if (!m.isTemDica()) {
            System.out.println("Ativar Dicas? (sim/nao)?");
            Resposta = s.next();
            //Se sim irá mostrar que as dicas estão ativadas!!!
            while (!Resposta.equalsIgnoreCase("sim") && !Resposta.equalsIgnoreCase("nao")) {
                    System.out.println("Informe uma opção valida!!!:");
                    Resposta = s.next();
            }
            if (Resposta.equalsIgnoreCase("sim")) {
                System.out.println("-----------------------------------");
                System.out.println("----------Dica Ativadas!-----------");
                System.out.println("-----------------------------------");
                m.ativarDicas(true);
            } else {
                System.out.println("-----------------------------------");
                System.out.println("--------Dica Desativadas!----------");
                System.out.println("-----------------------------------");
            }
        }
        //O jogo será executado enquanto o metodo perdeu() retornar FALSE
        do {
            //Aqui é impresso a situação atual do jogo, numero de erros e acertos, 
            //além e não menos importante, a pontuação do jogador
            System.out.println(m);
            System.out.println("Qual é a palavra? " + m.getPalavraEmbalharada());
            System.out.print("Resposta: ");
            Resposta = s.next();
            //o Método jogar(Resposta) recebe como argumento a resposta do jogado, 
            //que logo verifica a resposta estar correta
            m.jogar(Resposta);
            //Se estiver correta, o bloco asseguir é ignorado
            //Se estiver errada, isso é verificada através do método isError(), 
            //e as dicas estiverem ativadas isso é verificada através do isTemDica()
            //o bloco abaixo é executado
            if (m.isError() && m.isTemDica()) {
                //aqui é perguntado ao usuário se ele que visualizar a dica
                System.out.print("Visualizar dica?(sim/nao)");
                Resposta = s.next();
                while (!Resposta.equalsIgnoreCase("sim") && !Resposta.equalsIgnoreCase("nao")) {
                    System.out.println("Informe uma opção valida!!!:");
                    Resposta = s.next();
                }
                //se sim o progrma mostra a dica,
                if (Resposta.equalsIgnoreCase("sim")) {
                    System.out.println("-----------------------------------");
                    System.out.println("Dica: " + m.getDica());
                    System.out.println("-----------------------------------");
                    System.out.println("Qual é a palavra? ");
                    System.out.print("Resposta: ");
                    Resposta = s.next();
                    //a resposta é verificada novamente
                    m.jogar(Resposta);
                    //se a dica estiver correta, é somado mais um na variável
                    //que armazena as dicas acertada
                    m.setAcertouDica();
                } else {
                    //se o usuário não quis a dica, ou informou um valor inválido
                    //a menssagem abaixo é impressa
                    System.out.println("Perdeu a chance!!!");
                }

            }
            //se o usuário errou a resposta o bloco abaixo é executado
            if (m.isError()) {
                //a quatidade de erros é atualizada através do metodo setErros
                m.setErros();
                //e a palvra correta é exibida logo após
                System.out.println("-----------------------------------\n"
                        + "A palavra correta era:"
                        + m.getPalavra());
            } else {
                //se o usuário não errou,a quantidade de acertos é atualizada
                m.setAcertos();
            }
            //Aqui é verificado se o usuário perdeu
            if (m.perdeu()) {
                
                //se sim a mesnsagem abaixo é impressa no console
                System.out.println("\n***********************************");
                System.out.println("----------- GAME OVER  ------------");
                System.out.println("***********************************\n");
                //Aqui impresso o resultadoo final do jogo
                System.out.println(m);
                //Aqui o usuário tem a opção de reiniciar o jogo
                System.out.print("Vc deseja reiniciar o atual jogo?(sim/nao)");
                Resposta = s.next();
                //caso o usurio dê uma resposta invalida o programa vai pedir uma resposta valida
                while (!Resposta.equalsIgnoreCase("sim") && !Resposta.equalsIgnoreCase("nao")) {
                    System.out.println("Informe uma opção valida!!!:");
                    Resposta = s.next();
                }
                if (Resposta.equalsIgnoreCase("sim")) {
                    m.reiniciarJogo();
                    System.out.println("\n***********************************");
                    System.out.println("----------- NOVO JOGO  ------------");
                    System.out.println("***********************************\n");
                } else if (Resposta.equalsIgnoreCase("nao")) {
                    System.out.println("\nJogo encerrado");
                }
            }
            //uma nova palavra embaralhada é gerada para o usuário
            m.novaPalavra();
            //Condição de loop While: se o usuário perdeu(true) ele encerra o loop
            //se não(false) o jogo continua
        } while (!m.perdeu());
    }
}
