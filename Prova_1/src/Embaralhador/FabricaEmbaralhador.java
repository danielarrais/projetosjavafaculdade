package Embaralhador;


import Embaralhador.Interfaces.Embaralhador;
import java.util.ArrayList;
import java.util.Random;

public class FabricaEmbaralhador {
    //Uma arrayList é criada para armazenar todos os embaralhadores implementados
    private static ArrayList<Embaralhador> array = new ArrayList<>();
    //Variavel do Tipo Ramdom, que será usada para adquirir
    //um valor aleatório em um determinado intervalho
    private static Random r = new Random();
    //Metodo estatico que retorna uma instância aleátória armazenada no arrayList
    public static Embaralhador retorno() {
        //Instâncias são adicionadas ao arrayList
        array.add(new EmbaralhaLetras());
        array.add(new EmbaralhadorAstesrisk());
        //O método retorna o conteúdo do arrayList usando um indíce aleatório
        return array.get(r.nextInt(array.size()));
    }
}
