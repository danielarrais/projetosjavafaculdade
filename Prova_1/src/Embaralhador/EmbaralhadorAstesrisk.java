package Embaralhador;

import Embaralhador.Interfaces.Embaralhador;

public class EmbaralhadorAstesrisk implements Embaralhador {

    //Array que armazena as vogais para posterior substituição
    char[] vogais = {'A', 'E', 'I', 'O', 'U'};
    //Metodo que troca as vogais da String passada como arqumento por *
    public String embaralha(String palavra) {
        for (int i = 0; i < vogais.length; i++) {
            //Convete as possições char do meu array em String
            String vogal = Character.toString(vogais[i]);
            //Coloca a palavra em caixa alta
            palavra = palavra.toUpperCase();
            //Substitui qualquer vogal por *
            palavra = palavra.replaceAll(vogal, "*");
        }
        //Retorna a palavra sem as vogais
        return palavra;
    }
}
