package Embaralhador;
import Embaralhador.Interfaces.Embaralhador;
import java.util.*;
public class EmbaralhaLetras implements Embaralhador {
    //Variavel do Tipo Ramdom, que será usada para adquirir
    //um valor aleatório em um determinado intervalho
    private static Random r = new Random();
    //Metodo que retornará uma palara com suas letras embaralhadas de forma randômica
    public String embaralha(String palavra) {
        //Transformação do argumento em um array do tipo char
        char array[] = palavra.toCharArray();
        String s = "";
        for (int i = 0; i < (array.length); i++) {
            //aqui é armazenado um numero aleatório que varia entre 0 e o tamanho do array
            int aleatorio = r.nextInt(array.length);
            //O conteúdo da posição atual do array é armazenado,
            //para evitar perdas durante a posterior troca
            char ant = array[i];
            //a posição atual do array recebe a posição com o indice aleátorio armazenado anteriormente
            array[i] = array[aleatorio];
            //e a posição referente ao indice aleátorio recebe o conteúdo armazenado em ant
            array[aleatorio] = ant;
//            if (array[i]%2==0) {
//                char ant = array[i-1];
//                array[i-1] = array[i];
//                array[i] = ant;
//            }
        }
        
        //esse for percorre o array embaralhado concatenando seu conteúdo em uma string
        for (int i = 0; i < array.length; i++) {
            s = s + array[i];
        }
        //A variável s é retornada com a palavra embaralhada
        return s;
    }
}
