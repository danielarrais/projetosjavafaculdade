public class Conta {
    private String Cliente;
    private int Numero;
    private String Tipo;
    
    private float Saldo;

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }
    
    public float retornarSaldo() {
        return Saldo;
    }

    public void Depositar(float Valor) {
        this.Saldo = Saldo + Valor;
        System.out.println("Você depositou " + Valor +" Reais");
    }
    
    public void Sacar(float valor){
        if (valor <= Saldo) {
            Saldo = Saldo - valor;
        }
    }
    public Conta(String Cliente, int Numero, String Tipo) {
        setCliente(Cliente);
        setNumero(Numero);
        setTipo(Tipo);
    }
}
