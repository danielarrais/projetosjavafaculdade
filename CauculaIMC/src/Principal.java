public class Principal {
    
    public static void main(String[] args) {
        Paciente p1= new Paciente("Daniel", 20.00f, 1.7f);
        Paciente p2= new Paciente("Paulo", 70.00f, 1.7f);
        Paciente p3= new Paciente("João", 350.00f, 1.7f);
        
        System.out.println(p1.getNome()+" você está "+p1.diagnostico());
        System.out.println(p2.getNome()+" você está "+p2.diagnostico());
        System.out.println(p3.getNome()+" você está "+p2.diagnostico());
    }
}
