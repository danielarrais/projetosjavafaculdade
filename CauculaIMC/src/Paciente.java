public class Paciente {

    private String nome;
    private float peso;
    private float altura;
    private float imc;

    public float getPeso() {
        return peso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public Paciente(String nome, float peso, float altura) {
        setNome(nome);
        setAltura(altura);
        setPeso(peso);
    }

    public float calculoIMC() {
        imc = getPeso()/(getAltura() * getAltura());
        return imc;
    }

    public String diagnostico() {
        if (calculoIMC() < 16.00f) {
            return "baixo peso muito grave";
        } else if (calculoIMC() > 16.00f && calculoIMC()<16.99f) {
            return "baixo peso grave";
        } else if (calculoIMC() > 17.00f && calculoIMC()<18.49f) {
            return "baixo peso";
        } else if (calculoIMC() > 18.50f && calculoIMC()<24.99f) {
            return "peso normal";
        } else if (calculoIMC() > 25.00f && calculoIMC()<29.99f) {
            return "sobrepeso";
        } else if (calculoIMC() > 30.00f && calculoIMC()<34.99f) {
            return "obesidade grau I ";
        } else if (calculoIMC() > 35.00f && calculoIMC()<39.99f) {
            return "obesidade grau II ";
        } else if (calculoIMC() > 40f) {
            return "obesidade grau III (obesidade mórbida)";
        } else {
            return null;
        }
    }
}
