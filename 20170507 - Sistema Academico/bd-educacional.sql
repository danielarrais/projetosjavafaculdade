DROP TABLE IF EXISTS professores;
DROP TABLE IF EXISTS universidades;
DROP TABLE IF EXISTS professoresUniversidades;

CREATE TABLE professores 
(
   idProfessor INT NOT NULL AUTO_INCREMENT,
   nome VARCHAR (20) NOT NULL,
   sobrenome VARCHAR (20) NOT NULL,
   PRIMARY KEY (idProfessor)
);

CREATE TABLE universidades 
(
   sigla varchar (20) NOT NULL,
   nome varchar (50) NOT NULL,
   estado varchar (2) NOT NULL,
   PRIMARY KEY (sigla)
);

CREATE TABLE professoresUniversidades 
(
   idProfessor INT NOT NULL,
   sigla VARCHAR (20) NOT NULL,
   FOREIGN KEY (idProfessor) REFERENCES professores (idProfessor), 
   FOREIGN KEY (sigla) REFERENCES universidades (sigla)
);

INSERT INTO professores (nome, sobrenome)
VALUES 
   ('Silvano','Malfatti'), 
   ('Paulo','Trenhago'),
   ('Douglas','Chagas'), 
   ('Augusto','Rezende'),
   ('Alexandre','Rossini'),
   ('Vinicius','Rios');

INSERT INTO universidades (sigla, nome, estado)
VALUES
   ('FACTO', 'Faculdade Catolica do Tocantins', 'TO'),
   ('UNITINS', 'Universidade do Tocantins', 'TO'),
   ('IFTO', 'Instituto Federal do Tocantins', 'TO'),
   ('UFT', 'Universidade Federal do Tocantins', 'TO');

INSERT INTO professoresUniversidades (idProfessor, sigla)
VALUES
   (1,'FACTO'),
   (1,'UNITINS'),
   (2,'FACTO'),
   (2,'UNITINS'),
   (3,'UNITINS'),
   (4,'UNITINS'),
   (5,'UFT'),
   (6,'IFTO');
