
public class Principal {

	public static void main(String[] args) {
	
		Lanchonete vrLanchonete = new Lanchonete();
		
		//PEDIDOS======
		Produto vrProduto = new Pizza(Pizza.G, 4);
		vrProduto.nome = "Pizza Mussarela";
		vrProduto.codigo = 20;
		vrLanchonete.recebePedido(vrProduto);
		
		
		vrProduto = new Suco(Suco.CREME);
		vrProduto.nome = "Creme Morango";
		vrProduto.codigo = 30;
		vrLanchonete.recebePedido(vrProduto);
		
		
		vrProduto = new Tapioca(3);
		vrProduto.nome = "Tapioca Queijo";
		vrProduto.codigo = 40;
		vrLanchonete.recebePedido(vrProduto);
		vrLanchonete.recebePedido(vrProduto);
		vrLanchonete.recebePedido(vrProduto);
		
	vrLanchonete.mostraComanda();
	}

}
