package views;

import java.awt.*;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;

import Listas.Fila;
import Listas.LinkedList;
import Listas.Pilha;

import javax.swing.*;

//import Listas.LinkedList;
import controllers.ctrlServidor;
import models.CrudServidor;
import models.GuicheThread;
import models.PainelAtendimentoThread;

public class Servidor extends JFrame {

	private static final long serialVersionUID = 1L;

	// Fila para controle
	Fila<String> fila = new Fila<>();

	// Atributos do servidor
	public JTextArea areaTexto = null;

	// componentes diversos de interface
	JTextField campoTexto = null;
	JButton botaoOk = null;

	// server's sockets para receber conex�es dos clientes
	ServerSocket servidorGuiche = null;
	ServerSocket servidorAtendimento = null;

	// int�ncia do crud para manipular o banco de dados
	public CrudServidor crud = new CrudServidor();

	// salva instancia do servidor para uso em classes anonimas
	Servidor servidor = this;

	// int�ncia para controle de tecla do servidor
	ctrlServidor controller = new ctrlServidor(this);

	// listas para guardar as conex�es dos clientes
	public LinkedList<GuicheThread> guicheThread = new LinkedList<GuicheThread>();
	public LinkedList<PainelAtendimentoThread> atendimentoThread = new LinkedList<PainelAtendimentoThread>();

	// variaveis para guardar resultados das consultas ao banco
	public String senhasChamdas, senhasNChamadas;
	public String proximaCategoria = "P";

	// Run() para enviar informa��es aos clientes
	Runnable senhasChamadas = new Runnable() {

		public void run() {
			// diz que houve altera��o no banco, para que ocorra uma primeira
			// consulta
			CrudServidor.HOUVEALTERACAO = true;

			while (true) {
				// verifica se houve altera��o no banco de dados antes de fazer
				// alguma consulta
				if (CrudServidor.HOUVEALTERACAO) {
					// captura senhas que n�o foram chamadas, diretamente do
					// banco de dados
					senhasNChamadas = crud.listarSenhas();
					// captura senhas que foram chamadas, diretamente do banco
					// de dados
					senhasChamdas = crud.listarSenhasChamadas();

					CrudServidor.HOUVEALTERACAO = false;
				}

				// variaveis para saber se determinadas categorias de senha
				// foram ou n chamadas
				boolean temP = senhasNChamadas.contains("P");
				boolean temC = senhasNChamadas.contains("C");
				boolean temJ = senhasNChamadas.contains("J");

				// verifica se h� senhas, se h� senhas relacionadas a procima
				// categoria
				if ((temJ || temC || temP) && !senhasNChamadas.contains(proximaCategoria)) {

					// se n�o ele atualiza a ordem at� encontrar
					while (!senhasNChamadas.contains(proximaCategoria)) {
						atualizaOrdem();
					}
				}

				// envia pacotes de dados(separados por "-"),
				// com seus devidos cabe�alhos e dados (cabesalhos e dados
				// separados por ":", e dados separados por " ")
				String mensagem = "CATEGORIA:" + proximaCategoria + "-SC:" + senhasChamdas.trim() + "-SNC:"
						+ senhasNChamadas.trim();

				// percorre todo o array de clientes, enviando as mensagens
				for (int i = 0; i < guicheThread.size(); i++) {
					GuicheThread guicheThreadT = guicheThread.busca(i);
					guicheThreadT.enviaMensagem(mensagem);
				}

				// atraso na Thread
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
				}
			}

		}
	};

	// runnable que captura conex�es dos guiches de senha
	Runnable rServidorGuiches = new Runnable() {
		@Override
		public void run() {
			while (true) {
				try {
					Socket clientePainel = servidorGuiche.accept();
					GuicheThread painelThread = new GuicheThread(clientePainel, servidor);
					guicheThread.add(painelThread);
				} catch (Exception e) {

				}
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
				}
			}
		}
	};

	// runnable que captura conex�es dos paineis de senha
	Runnable rServidorAtendimento = new Runnable() {
		@Override
		public void run() {
			while (true) {
				try {
					Socket clientePainel = servidorAtendimento.accept();
					PainelAtendimentoThread painelThread = new PainelAtendimentoThread(clientePainel, servidor);
					atendimentoThread.add(painelThread);
				} catch (Exception e) {
				}
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
				}
			}
		}
	};

	Servidor() {
		// Configuracoes da janela
		setSize(500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Servidor aguardando...");
		setLayout(new BorderLayout());

		// Criacao dos objetos visuais
		campoTexto = new JTextField(35);
		botaoOk = new JButton("OK");
		botaoOk.addActionListener(controller);
		areaTexto = new JTextArea(10, 10);
		JScrollPane borda = new JScrollPane(areaTexto);

		// Adiciona os componenentes a janela
		add(borda, BorderLayout.CENTER);
		JPanel painel = new JPanel();
		painel.setLayout(new FlowLayout());
		painel.add(campoTexto);
		painel.add(botaoOk);
		add(painel, BorderLayout.SOUTH);

		// Cria o objeto servidor com a porta
		// Valida os canais a partir do socket cliente
		try {
			// cria��o do servidor que vai trata os guiches e os paines de senha
			servidorGuiche = new ServerSocket(3088);

			// cria��o do servidor que vai trata os o auto-atendimento
			servidorAtendimento = new ServerSocket(3098);

			// inicializa��o das Threads:
			new Thread(rServidorGuiches).start();
			new Thread(senhasChamadas).start();
			new Thread(rServidorAtendimento).start();
			crud.limparTabela("senhas_retiradas");
			crud.limparTabela("ultima_chamada");
		} catch (Exception e) {
		}

		// detalha��o da ordem de chamadas
		fila.add("P");
		fila.add("J");
		fila.add("P");
		fila.add("C");
		setVisible(false);
	}

	// M�todo que dita qual categoria vai ser chamada
	public void atualizaOrdem() {

		// pega troca o conteudo do come�o da fila para o fim e vice versa
		fila.inserir(fila.remover());

		// seta a proxima categoria a ser chamada
		proximaCategoria = fila.buscar();
	}

	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						DatagramSocket datagramSocket = new DatagramSocket();
						byte[] buffer = "SERVERFE".getBytes();
						InetAddress receiverAddress = InetAddress.getLocalHost();
						DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress, 80);
						datagramSocket.send(packet);
						datagramSocket.close();
						packet = null;
						datagramSocket = null;
						Thread.sleep(50);
					} catch (IOException | InterruptedException e) {
					}
				}
			}
		}).start();
		new Servidor();
	}
}
