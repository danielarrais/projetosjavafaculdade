package models;

public class Senha {
	String categoria;
	int senha;
	
	//getters
	public String getCategoria() {
		return categoria;
	}
	
	public int getSenha() {
		return senha;
	}
	
	//setters
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public void setSenha(int senha) {
		this.senha = senha;
	}
	
	//metodo principal
	public Senha(String categoria, int senha)
	{
		setCategoria(categoria);
		setSenha(senha);
	}
	
}
