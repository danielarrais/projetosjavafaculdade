package models;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Scanner;
import Listas.LinkedList;

public class CrudServidor {
	Connection conexao = null;
	String sql = null;
	private static String SERVER = "jdbc:mysql://localhost:3306/filaesperta?useSSL=false";
	private static String USER = "root";
	private static String PASSWORD = "root";
	public static boolean HOUVEALTERACAO;
	
	// metodo para cadastro de guiches no banco de dados
	public void cadastrarGuiche(Guiche g) {
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "INSERT INTO `filaesperta`.`guiches` (`id_guiche`, `aberto`) VALUES (?, ?);";
			PreparedStatement cadastro = conexao.prepareStatement(sql);
			cadastro.setInt(1, g.id);
			cadastro.setInt(2, 0);
			cadastro.execute();
			cadastro.close();
			conexao.close();
		} catch (Exception e) {
			//System.out.println(e.getMessage());
		}
	}

	// metodo para abrir um guiche apos ele ser criado
	public void abrirGuiche(Guiche g) {
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "UPDATE `filaesperta`.`guiches` SET `aberto`=? WHERE `id_guiche`=?;";
			PreparedStatement cadastro = conexao.prepareStatement(sql);
			cadastro.setInt(1, 1);
			cadastro.setInt(2, g.id);
			cadastro.execute();
			cadastro.close();
			conexao.close();
		} catch (Exception e) {
			//System.out.println(e.getMessage());
		}
	}

	// metodo para fechar um guiche que esta aberto ate o momento
	public void fecharGuiche(Guiche g) {
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "UPDATE `filaesperta`.`guiches` SET `aberto`=? WHERE `id_guiche`=?;";
			PreparedStatement cadastro = conexao.prepareStatement(sql);
			cadastro.setInt(1, 0);
			cadastro.setInt(2, g.id);
			cadastro.execute();
			cadastro.close();
			conexao.close();
		} catch (Exception e) {
			//System.out.println("Erro fechar guiche senha: " + e.getMessage());
		}
	}
	
		// metodo para os guiches chamarem as senhas
		public void chamarSenha(Senha senha, Guiche guiche) {
			try {
				conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
				String sql = "UPDATE `filaesperta`.`senhas_retiradas` SET `chamada`='1', `guiche`=?, `horario_r`=? WHERE `senha`=?;";
				PreparedStatement cadastro = conexao.prepareStatement(sql);
				cadastro.setInt(1, guiche.getId());
				cadastro.setTimestamp(2, getCurrentJavaSqlTimestamp());
				cadastro.setInt(3, senha.senha);
				cadastro.execute();
				cadastro.close();
				cadastro.close();
				conexao.close();
				HOUVEALTERACAO = true;
			} catch (Exception e) {
				System.out.println("Erro chamarSenha: " + e.getMessage());
			}
		}

	// metodo para criar uma senha ap�s o cliente acessar o gerador de senhas
	public Senha novaSenha(String senha) {
		Senha s = null;
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "INSERT INTO `filaesperta`.`senhas_retiradas` (`categoria`, `horario`) VALUES (?, ?);";
			PreparedStatement cadastro = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			cadastro.setString(1, senha);
			cadastro.setTimestamp(2, getCurrentJavaSqlTimestamp());
			cadastro.execute();
			
			ResultSet retornoValor = cadastro.getGeneratedKeys();
			while (retornoValor.next()) {
				s = new Senha(senha, retornoValor.getInt(1));
			}
			cadastro.close();
			conexao.close();
			HOUVEALTERACAO = true;
			HOUVEALTERACAO = true;
		} catch (Exception e) {
			//System.out.println("Erro nova senha: " + e.getMessage());
		}
		return s;
	}
	// metodo para os guiches chamarem as senhas chamada
	public String listarSenhasChamadas() {
		String senha = "";
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "select * from senhas_retiradas where chamada = 1 ORDER BY horario_r ASC;";
			PreparedStatement consulta = conexao.prepareStatement(sql);
			ResultSet resultado = consulta.executeQuery();

			while (resultado.next()) {
				String senhaN = resultado.getObject(1) + "";
				for (int i = 1; senhaN.toCharArray().length < 4; i++) {
					senhaN = "0" + senhaN;
				}
				senha = senha + " " + resultado.getObject(2) + senhaN+";"+resultado.getObject(4);
			}
			resultado.close();
			consulta.close();
			conexao.close();
		} catch (Exception e) {
			System.out.println("Deu PAU!! no listarSenhasChamadas" + e.getMessage());
		}

		return senha.trim();
	}
	// metodo para limpar tabelas
	public void limparTabela(String tabela) {
		try {
			
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "TRUNCATE TABLE "+tabela+";";
			Statement cadastro = conexao.prepareStatement(sql);
			cadastro.execute(sql);
			cadastro.close();
			conexao.close();
		} catch (Exception e) {
			//System.out.println("Erro limparTabela: " + e.getMessage());
		}
	}

	public static java.sql.Timestamp getCurrentJavaSqlTimestamp() {
		java.util.Date date = new java.util.Date();
		return new java.sql.Timestamp(date.getTime());
	}

	
	
	// metodo para os trazer do banco as senhas não chamadas
	public String listarSenhas() {
		String senha = "";
		try {
			conexao = DriverManager.getConnection(SERVER, USER, PASSWORD);
			String sql = "select senha,categoria,horario from senhas_retiradas where chamada != 1 ORDER BY horario ASC;";
			PreparedStatement consulta = conexao.prepareStatement(sql);
			ResultSet resultado = consulta.executeQuery();
			ResultSetMetaData metaDados = resultado.getMetaData();

			int numeroColunas = metaDados.getColumnCount();
			while (resultado.next()) {
				String senhaN = resultado.getObject(1) + "";
				for (int i = 1; senhaN.toCharArray().length < 4; i++) {
					senhaN = "0" + senhaN;
				}
				senha = senha + " " + resultado.getObject(2) + senhaN;
			}
			resultado.close();
			consulta.close();
			conexao.close();
		} catch (Exception e) {
			//System.out.println("Deu PAU!! no listarSenhas" + e.getMessage());
		}
		return senha.trim();
	}
}
