package Listas;

import java.util.ArrayList;

public class Fila<T> extends ArrayList<T> {
    public void inserir(T conteudo) {
        super.add(0, conteudo);
    }
    public T remover() {
        return (T) super.remove(super.size()-1);
    }
    public T buscar(){
    	return (T) super.get(super.size()-1);
    }

    @Override
    public T get(int index) {
        if (index<this.size()) {
            return super.get(index);
        }else{
            return null;
        }
    }
    
    
}