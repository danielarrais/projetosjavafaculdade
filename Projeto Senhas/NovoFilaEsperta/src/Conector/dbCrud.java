package Conector;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.Senha;

public class dbCrud {

    Connection con = null;

    boolean bancoCriado = false;

    public static Connection ConectorBD() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + System.getenv("ProgramFiles") + "\\FilaEsperta\\DB\\BDSenhas.sqlite");
            return con;
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Contate o suporte para solução do problema "
                    + "\n" + "(063)98428-0882");
            return null;
        }
    }

    public ArrayList<Senha> senhasNaoChamadas() throws SQLException {
        Connection c = ConectorBD();
        Statement stmt = null;
        ArrayList<Senha> senhas = new ArrayList<>();
        try {
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM senhas_retiradas WHERE chamada=1 ORDER BY horario_r ASC");
            while (rs.next()) {
                senhas.add(new Senha(rs.getInt("senha"), rs.getString("categoria"), rs.getInt("chamada"), rs.getString("horario"), rs.getString("horario_r")));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return senhas;
    }

    public ArrayList<Senha> senhaEPedidos() throws SQLException {
        Connection c = ConectorBD();
        Statement stmt = null;
        ArrayList<Senha> senhas = new ArrayList<>();
        try {
            c.setAutoCommit(false);
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM senhas_retiradas");
            while (rs.next()) {
                senhas.add(new Senha(rs.getInt("senha"), rs.getString("categoria"), rs.getInt("chamada"), rs.getString("horario"), rs.getString("horario_r")));
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return senhas;
    }

    public void createDB() throws SQLException {
        Connection c = ConectorBD();
        Statement prepareStatement = null;
        if (bancoCriado) {
            System.out.println("Banco já existe!!!");
        } else {
            try {
                prepareStatement = c.createStatement();
                String sql = "CREATE TABLE `senhas_retiradas` (\n"
                        + "  `senha` unsigned int(4) NOT NULL,\n"
                        + "  `categoria` varchar(1) NOT NULL,\n"
                        + "  `chamada` tinyint(1) NOT NULL DEFAULT '0',\n"
                        + "  `horario` TIMESTAMP DEFAULT NULL,\n"
                        + "  `horario_r` TIMESTAMP DEFAULT NULL,\n"
                        + "  PRIMARY KEY (senha, categoria)"
                        + ")";
                prepareStatement.executeUpdate(sql);
                prepareStatement.close();
                c.close();
                bancoCriado = true;
            } catch (SQLException e) {
            }
        }

    }

    public void inserirPedido(Senha senha) throws SQLException {
        Connection c = ConectorBD();
        PreparedStatement prepareStatement = null;
        try {
            c.setAutoCommit(false);
            String sql = "insert into senhas_retiradas(senha, categoria, horario) values (?,?,datetime('now'));";
            prepareStatement = c.prepareStatement(sql);
            prepareStatement.setInt(1, senha.getSenha());
            prepareStatement.setString(2, senha.getCategoria());
            prepareStatement.execute();
            prepareStatement.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
    public Senha novaSenha() throws SQLException {
        Connection c = ConectorBD();
        Senha senhaInserida = null;
        PreparedStatement prepareStatement = null;
        try {
            c.setAutoCommit(false);
            String sql = "insert into senhas_retiradas(categoria, horario) values (?,datetime('now'));";
            prepareStatement = c.prepareStatement(sql);
            prepareStatement.setString(1, "M");
            prepareStatement.execute();
            prepareStatement.close();
            c.commit();
            c.close();
            ResultSet retornoValor = prepareStatement.getGeneratedKeys();
            while (retornoValor.next()) {
                senhaInserida = new Senha(retornoValor.getInt(1), retornoValor.getString(2));
            }

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return senhaInserida;
    }

    public void chamarSenha(Senha senha) throws SQLException {
        Connection c = ConectorBD();
        PreparedStatement prepareStatement = null;
        try {
            c.setAutoCommit(false);
            String sql = "update senhas_retiradas SET chamada = ?, horario_r=datetime('now') WHERE senha=? AND categoria = ? ;";
            prepareStatement = c.prepareStatement(sql);
            prepareStatement.setInt(1, 1);
            prepareStatement.setInt(2, senha.getSenha());
            prepareStatement.setString(3, senha.getCategoria());
            prepareStatement.execute();
            c.commit();
            prepareStatement.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public static java.sql.Timestamp getCurrentJavaSqlTimestamp() {
        java.util.Date date = new java.util.Date();
        return new java.sql.Timestamp(date.getTime());
    }

    public static void main(String[] args) throws SQLException {
        dbCrud c = new dbCrud();
        ArrayList<Senha> senhas = new ArrayList<>();
        c.createDB();
        long mil = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            c.inserirPedido(new Senha(i, "M"));
            c.inserirPedido(new Senha(i, "P"));
            System.out.println(i);
        }
        mil = (System.currentTimeMillis() - mil);
        System.out.println(mil / 1000);
    }
}
