package models;

import java.util.LinkedList;

public class Guiche {
	int id = 0;
	LinkedList<String> senhaP = new LinkedList<>();
	LinkedList<String> senhaC = new LinkedList<>();
	LinkedList<String> senhaJ = new LinkedList<>();
	
	
	//getters
	public LinkedList<String> getSenhaP() {
		return senhaP;
	}
	
	public int getId() {
		return id;
	}

	public void setSenhaC(LinkedList<String> senhaC) {
		this.senhaC = senhaC;
	}
	
	public void setSenhaJ(LinkedList<String> senhaJ) {
		this.senhaJ = senhaJ;
	}

	//setters
	public void setId(int id) {
		this.id = id;
	}
	
	public void setSenhaP(LinkedList<String> senhaP) {
		this.senhaP = senhaP;
	}

	public LinkedList<String> getSenhaC() {
		return senhaC;
	}
	
	LinkedList<String> getSenhaJ() {
		return senhaJ;
	}
	

	//metodo principal
	public Guiche(int id, LinkedList<String> senhaP, LinkedList<String> senhaC, LinkedList<String> senhaJ)
	{
		setId(id);
		setSenhaC(senhaC);
		setSenhaJ(senhaJ);
		setSenhaP(senhaP);
	}

	public Guiche(Integer valueOf) {
		id = valueOf;
	}
	
}