package Views;

import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import Crontrollers.CtrlAberturaGuiche;

import javax.swing.JButton;
import javax.swing.JFrame;

public class PainelAberturaGuiche extends JFrame{
	
	//compontentes diversos de interface
	private JTextField guicheAberto;
	private JButton btnAbrirGuiche;
	private JLabel lblTituloJanela;
	private JPanel paneAberturaGuiche;
	private JLabel lblAlerta; 
	private CtrlAberturaGuiche abrirGuiche = null;
	
	//variavel para armazena dados inserido pelo usu�rio
	public Integer valorID = null;
	
	public JLabel getLblAlerta() {
		return lblAlerta;
	}

	//getters
	public JTextField getGuicheAberto() {
		return guicheAberto;
	}
	
	public JButton getBtnAbrirGuiche() {
		return btnAbrirGuiche;
	}

	//setters
	public void setGuicheAberto(JTextField guicheAberto) {
		this.guicheAberto = guicheAberto;
	}

	public void setBtnAbrirGuiche(JButton btnAbrirGuiche) {
		this.btnAbrirGuiche = btnAbrirGuiche;
	}

	//Construtor
	public PainelAberturaGuiche() {
		setResizable(false);
		setBounds(100, 100, 257, 304);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		paneAberturaGuiche = new JPanel();
		paneAberturaGuiche.setBackground(new Color(13, 82, 149));
		getContentPane().add(paneAberturaGuiche);
		paneAberturaGuiche.setLayout(null);
		
		lblTituloJanela = new JLabel("<html><center>Digite o numero do <br/>do seu guich�</center></html>");
		lblTituloJanela.setBounds(12, 11, 227, 102);
		paneAberturaGuiche.add(lblTituloJanela);
		lblTituloJanela.setForeground(Color.WHITE);
		lblTituloJanela.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblTituloJanela.setBackground(new Color(13, 82, 149));
		
		guicheAberto = new JTextField();
		guicheAberto.setBounds(94, 124, 62, 33);
		paneAberturaGuiche.add(guicheAberto);
		guicheAberto.setForeground(Color.BLACK);
		guicheAberto.setHorizontalAlignment(SwingConstants.CENTER);
		guicheAberto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		guicheAberto.setColumns(10);
		
		btnAbrirGuiche = new JButton("Abrir guich\u00EA");
		btnAbrirGuiche.setBounds(60, 168, 131, 33);
		paneAberturaGuiche.add(btnAbrirGuiche);
		btnAbrirGuiche.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAbrirGuiche.setForeground(Color.WHITE);
		btnAbrirGuiche.setBackground(new Color(255, 140, 0));
		
		//label para alerta
		lblAlerta = new JLabel("Informe um valor num\u00E9rico e inteiro!!!");
		lblAlerta.setForeground(Color.YELLOW);
		lblAlerta.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlerta.setBounds(0, 232, 251, 14);
		paneAberturaGuiche.add(lblAlerta);
		lblAlerta.setVisible(false);
		
		//metodo de acao do botao
		abrirGuiche = new CtrlAberturaGuiche(this);
		btnAbrirGuiche.addActionListener(abrirGuiche);

		setVisible(true);
	}
}
