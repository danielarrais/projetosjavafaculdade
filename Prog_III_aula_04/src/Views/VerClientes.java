package Views;

import Controllers.VerClasseController;
import Models.Cliente;
import Models.GerenteArmazenamento;
import arrayList.MeuArray;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class VerClientes extends JFrame {

    private JTable vrTabelaClientes = null;
    String[] nomes = {"Nomes", "Endereço", "Idade", "Sexo"};
    private JScrollPane tableScroll = null;
    private JPanel painel = null;
    JButton vrExcluir;
    JButton vrOk = null;
    JButton vrNovo = null;
    JButton vrAtualizar = null;

    VerClasseController controller = new VerClasseController(this);

    public VerClientes() throws FileNotFoundException{
        setTitle("Clientes Cadastrados");
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new FlowLayout());
        setResizable(false);
        
        painel = new JPanel();
        painel.setLayout(new GridLayout(1, 1));
        tableScroll = new JScrollPane(Tabela());
        painel.add(tableScroll);

        vrOk = new JButton("Ok");
        vrOk.addActionListener(controller);
        
        vrNovo = new JButton("Novo");
        vrNovo.addActionListener(controller);
        
        vrExcluir = new JButton("Excluir Selecionado");
        vrExcluir.addActionListener(controller);

        vrAtualizar = new JButton("Atualizar");
        vrAtualizar.addActionListener(controller);

        getContentPane().add(painel);
        add(vrOk);
        add(vrNovo);
        add(vrExcluir);
        add(vrAtualizar);

        setVisible(true);
    }

    public JButton getVrOk() {
        return vrOk;
    }

    public JButton getVrNovo() {
        return vrNovo;
    }

    public JButton getVrAtualizar() {
        return vrAtualizar;
    }

    public JScrollPane getTableScroll() {
        return tableScroll;
    }
    
    public JTable Tabela() throws FileNotFoundException{
    
       // MeuArray<Cliente> clientes = ;
        String[][] dadosTabela = new String[GerenteArmazenamento.buscarCliente().tamanhoDaLista()][4];
        
        for (int i = 0; i < GerenteArmazenamento.buscarCliente().tamanhoDaLista(); i++) {
            String[] cliente = new String[4];
            cliente[0] = GerenteArmazenamento.buscarCliente().buscarElemento(i).getNome();
            cliente[1] = GerenteArmazenamento.buscarCliente().buscarElemento(i).getEndereco();
            cliente[2] = String.valueOf(GerenteArmazenamento.buscarCliente().buscarElemento(i).getIdade());
            cliente[3] = String.valueOf(GerenteArmazenamento.buscarCliente().buscarElemento(i).getSexo());
            dadosTabela[i] = cliente;
        }
        vrTabelaClientes = new JTable(dadosTabela, nomes);
    
        return vrTabelaClientes;
    }    

    public JTable getVrTabelaClientes() {
        return vrTabelaClientes;
    }

    public JPanel getPainel() {
        return painel;
    }

    public void setTableScroll(JScrollPane tableScroll) {
        this.tableScroll = tableScroll;
    }

    public JButton getVrExcluir() {
        return vrExcluir;
    }
    
    
}
