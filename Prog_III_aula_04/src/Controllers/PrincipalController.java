package Controllers;

import Views.TelaCadastro;
import Views.TelaPrincipal;
import Views.VerClientes;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

public class PrincipalController implements ActionListener {

    private TelaPrincipal tela = null;
    private VerClientes telaV = null;
    private TelaCadastro telaC = null;

    public PrincipalController(TelaPrincipal copia) {
        tela = copia;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Point p = null;
        if (e.getSource() == tela.getSair()) {
            System.exit(1);
        } else if (e.getSource() == tela.getCadastrar()) {
            if (telaC != null) {
                p = telaC.getLocation();
                telaC.dispose();
            }
            telaC = null;
            telaC = new TelaCadastro();
            if (p != null) {
                telaC.setLocation(p);
            }
            telaC.setVisible(true);
        } else if (e.getSource() == tela.getVerClientes()) {
            try {
                if (telaV != null) {
                    p = telaV.getLocation();
                    telaV.dispose();
                }
                telaV = null;
                telaV = new VerClientes();
                if (p != null) {
                    telaV.setLocation(p);
                }
                telaV.setVisible(true);
            } catch (FileNotFoundException ex) {
                System.out.println("Eroor!");
            }
        }
    }

}
