package Controllers;

import Models.Cliente;
import Models.GerenteArmazenamento;
import Views.TelaCadastro;
import Views.VerClientes;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class CadastroController implements ActionListener {

    private TelaCadastro tela = null;

    public CadastroController(TelaCadastro copia) {
        tela = copia;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == tela.getVrBOk()) {
            boolean error = false;
            String erro = null;
            Cliente c = new Cliente();
            if (tela.getVrMaculino().isSelected() == false && tela.getVrFeminino().isSelected() == false) {
                erro = "Selecione o sexo!";
                error = true;
            } else {
                c.setSexo(tela.getVrMaculino().isSelected() ? 'M' : 'F');
            }
            if (tela.getVrSelecaoIdade().getSelectedIndex() == 0) {
                erro = "Idade inválida";
                error = true;
            } else {
                c.setIdade(tela.getVrSelecaoIdade().getSelectedIndex());
            }
            if (tela.getVrCampoEndereço().getText().equals("") || tela.getVrCampoEndereço().getText() == null) {
                erro = "Endereço inválido ";
                error = true;
            } else {
                c.setEndereco(tela.getVrCampoEndereço().getText());
            }

            if (tela.getVrCampoNome().getText().equals("") || tela.getVrCampoNome().getText() == null) {
                erro = "Nome inválido";
                error = true;
            } else {
                c.setNome(tela.getVrCampoNome().getText());
            }

            if (error) {
                tela.getErro().setText(erro);
            } else {
                Object[] options = {"Sim", "Não"};
                int opcao = JOptionPane.showOptionDialog(null,"Cadastrar "+tela.getVrCampoNome().getText()+" no sistema?", "Confirmar cadastro?", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                try {
                    if (opcao == 0) {
                        GerenteArmazenamento.gravaCliente(c);
                        JOptionPane.showMessageDialog(tela, c.getNome()+" cadastrado com sucesso!!!");
                        limparCampos();
                    }
                } catch (IOException ex) {
                    System.out.println("Error!");
                }

                if (tela.isAbrirCadastros()) {
                    tela.dispose();
                    try {
                        new VerClientes().setVisible(true);
                    } catch (FileNotFoundException ex1) {
                        Logger.getLogger(CadastroController.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }

            }

        } else if (e.getSource() == tela.getVrBCancelar()) {
            int opcao=0;
            if (comecouPreencher()) {
                Object[] options = {"Sim", "Não"};
                opcao = JOptionPane.showOptionDialog(null, "Sair", "Cancelar o cadastro??", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            }
            if (opcao==0) {
                tela.dispose();
            }
        } else if (e.getSource() == tela.getVrVercadastros()) {
            try {
                new VerClientes().setVisible(true);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(CadastroController.class.getName()).log(Level.SEVERE, null, ex);

            }
            tela.dispose();
        }
    }

    public void limparCampos() {
        tela.getVrCampoNome().setText("");
        tela.getVrCampoEndereço().setText("");
        tela.getVrSelecaoIdade().setSelectedIndex(0);
    }

    public boolean comecouPreencher() {
        boolean comecou = false;
        if (tela.getVrCampoNome().getText().equals("")==false) {
            comecou= true;
        } else if (tela.getVrCampoEndereço().getText().equals("")==false) {
            comecou= true;
        } else if (Integer.valueOf(tela.getVrSelecaoIdade().getSelectedItem().toString()) != 0) {
            comecou= true;
        }
        return comecou;
    }
    
    

}
