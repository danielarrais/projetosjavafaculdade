public class MeuArray {

    public static final int OFSSET = 1;
    int[] array;
    int ultimoInd = 0;

    public String getArray() {
        String arrayI = "";
        for (int i = 0; i < array.length; i++) {
            arrayI += array[i] + "  ";
        }
        return arrayI;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public MeuArray() {
        array = new int[OFSSET];
    }
    
    public void removeUltimoIndice(){
        ultimoInd --;
    }
    public void incrementaUltimoIndice(){
        ultimoInd ++;
    }
    
    public void add(int value) {
        int[] arrayTemp = null;
        if (array.length > ultimoInd) {
            array[ultimoInd] = value;
        } else {
            arrayTemp = new int[array.length + OFSSET];
            for (int i = 0; i < array.length; i++) {
                arrayTemp[i] = array[i];
            }
            arrayTemp[ultimoInd] = value;
            setArray(arrayTemp);
        }
        incrementaUltimoIndice();
    }
public void remove(int indice) {
        int[] arrayTemp;
        if (indice > ultimoInd) {
            System.out.println("Indice inválido!!!");
        } else if (indice == ultimoInd-1) {
            array[indice]=0;
        } else if(indice >= 0 && indice< ultimoInd) {
            for (int i = indice; i < array.length; i++) {
                if (i + 1 < array.length) {
                    array[i] = array[i + 1];
                }
            }
            removeUltimoIndice();
        } 
    }
   
    public static void main(String[] args) {
        MeuArray array = new MeuArray();
        array.add(0);
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        array.add(5);
        array.add(6);
        array.add(7);
        array.add(8);
        array.add(9);
        array.add(10);

        System.out.println(array.getArray());
        System.out.println(array.array.length);

        array.remove(10);

        System.out.println(array.getArray());
        System.out.println(array.array.length);
    }

}
