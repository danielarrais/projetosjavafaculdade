package estr_aula_06_recursividade;

public class Principal {

	// public void funcaoC(){
	// System.out.println("Inicio da da fun��o C");
	//
	// System.out.println("Fim da da fun��o C");
	// }
	// public void funcaoB(){
	// System.out.println("Inicio da da fun��o B");
	// funcaoC();
	// System.out.println("Fim da da fun��o B");
	// }
	// public void funcaoA(){
	// System.out.println("Inicio da da fun��o A");
	// funcaoB();
	// System.out.println("Fim da da fun��o A");
	// }
	public int fatorial(int i) {
		if (i == 0) {
			return 1;
		}

		return i * fatorial(i - 1);
	}

	public int potencia(int numero, int expoente) {
		if (expoente == 0)
			return 1;
		return numero * potencia(numero, expoente - 1);
	}

	public int fibonacci(int numero) {
		if (numero <= 2)
			return 1;
		
		return fibonacci(numero-1)+fibonacci(numero-2);

	}
	public int fibonacci2(int numero) {
		int fibonaci=0;
		int[] array = new int[numero];
		for (int i = 0; i <= numero; i++){
			if (i>1) {
				array[i]=array[i-1]-1 + numero-2;
			}
			
		}
		return array[numero];
	}
	public boolean isPalindromo(String palavra){
		if (palavra.length()-1 <=1 ) {
			return true;
		}else if(palavra.charAt(0)==palavra.charAt(palavra.length()-1)){
			return isPalindromo(palavra.substring(1, palavra.length()-2));
		}
		return false;
	}

	public static void main(String[] args) {
		Principal p = new Principal();
		// p.funcaoA();
		// System.out.println(p.fatorial(3));
		System.out.println(p.potencia(5, 10));
		System.out.println(p.fibonacci(7));
		System.out.println(p.isPalindromo("arara"));
	}
}
