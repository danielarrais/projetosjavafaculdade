
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MinhaJanela extends JFrame {

    Color c = new Color(0x000000);

    JTextArea texto = null;
    //Declara uma refer�ncia para um bot�o
    JButton bOk = null;
    JPanel jPanel1 = null;
    JPanel jPaneColar = null;
    JPanel jPaneColarGrande = null;

    JPanel cutCrop = null;
    JPanel fonts = null;

    JPanel anexo = null;

    JPanel fontMenores = null;
    JPanel textArea = null;
    JComboBox box = null;
    JComboBox boxSize = null;

    JButton colarGrande = null;
    JButton cut = null;
    JButton crop = null;

    JButton um = new JButton(new ImageIcon(getClass().getResource("maior.png")));
    JButton dois = new JButton(new ImageIcon(getClass().getResource("menor.png")));
    JButton tres = new JButton(new ImageIcon(getClass().getResource("und.png")));
    JButton quatro = new JButton(new ImageIcon(getClass().getResource("tax.png")));
    JButton cinco = new JButton(new ImageIcon(getClass().getResource("sub.png")));
    JButton seis = new JButton(new ImageIcon(getClass().getResource("sob.png")));

    JButton sete = new JButton(new ImageIcon(getClass().getResource("image.png")));
    JButton oito = new JButton(new ImageIcon(getClass().getResource("paint.png")));
    JButton nove = new JButton(new ImageIcon(getClass().getResource("data.png")));
    JButton dez = null;

    public String[] pegarFonts() {
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] fontes = e.getAllFonts();
        String[] f = new String[fontes.length];
        for (int i = 0; i < fontes.length; i++) {
            f[i] = fontes[i].getFontName().toString();
        }
        return f;
    }

    public MinhaJanela() throws ClassNotFoundException, InstantiationException, UnsupportedLookAndFeelException {

        //Titulo da Janela
        setTitle("Minha primeira janela");
        //Tamanho da Janela
        setSize(1280, 720);
        //Localiza��o inicial da Janela
        setLocation(0, 0);
        //Mudar cor do fundo da janela
        getContentPane().setBackground(c);
        //Altera opera��o de fechamento da janela
        //No caso, JFrame.EXIT_ON_CLOSE diz para o bot�o encerrar todo o sistema
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Permite ou n�o o redimensionamento
        setResizable(true);
        //Define a estrat�gia de layout a ser usada
        //setLayout(new FlowLayout());
        //Layout de grades
        //setLayout(new GridLayout(4, 10, 10, 10));
        //Layout de Borda
        setLayout(new BorderLayout(10, 10));
        //Intancia o novo painel
        jPanel1 = new JPanel();

        cutCrop = new JPanel();
        cutCrop.setLayout(new FlowLayout());

        jPanel1.setLayout(new GridLayout(0, 6, 0, 0));

        jPaneColar = new JPanel();
        jPaneColar.setLayout(new GridLayout(0, 2, 2, 2));

        cut = new JButton(new ImageIcon(getClass().getResource("cut.png")));
        cut.setBorder(null);
        crop = new JButton(new ImageIcon(getClass().getResource("copiar.png")));
        crop.setBorder(null);

        colarGrande = new JButton(new ImageIcon(getClass().getResource("colar.png")));
        colarGrande.setBorder(null);
        jPaneColarGrande = new JPanel(new FlowLayout());
        jPaneColarGrande.add(colarGrande);
        jPaneColar.add(jPaneColarGrande);
        jPaneColar.add(cutCrop);
        cutCrop.add(cut);
        cutCrop.add(crop);

        box = new JComboBox(pegarFonts());

        fonts = new JPanel(new GridLayout(2,0,3,3));
        fonts.add(box);
        Integer[] sises = {10,11,12,14,16,18,20};
        boxSize = new JComboBox(sises);

        //Int�ncia o bot�o
        bOk = new JButton("Confirmar");
        //bOk.setPreferredSize(new Dimension(100,20));
        jPanel1.add(jPaneColar);
        jPanel1.add(fonts);

        fontMenores = new JPanel(new FlowLayout());
        um.setBorder(null);
        fontMenores.add(um);
        dois.setBorder(null);
        fontMenores.add(dois);

        tres.setBorder(null);
        fontMenores.add(tres);

        quatro.setBorder(null);
        fontMenores.add(quatro);

        seis.setBorder(null);
        fontMenores.add(boxSize);

        fonts.add(fontMenores);
        anexo = new JPanel(new FlowLayout((int) LEFT_ALIGNMENT));
        sete.setBorder(null);
        anexo.add(sete);
        oito.setBorder(null);
        anexo.add(oito);
        nove.setBorder(null);
        anexo.add(nove);

        jPanel1.add(anexo);

        //Adiciona um componente visual � janela
//		for (int i=1; i<=40; i++) {
//			add(new JButton("Confirmar "+i));
//		}
        //O Segundo parem�tro s� � usado em layout do tipo Border
        textArea = new JPanel(new FlowLayout());
        texto = new JTextArea(50, 90);

        textArea.add(texto);
        add(jPanel1, BorderLayout.NORTH);
        add(textArea, BorderLayout.CENTER);

        //Torna todos os componentes vis�veis
        setVisible(true);
    }
}
