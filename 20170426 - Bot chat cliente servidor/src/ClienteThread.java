import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ClienteThread extends Thread {
	// Atributos da classe
	Socket cliente = null;
	DataInputStream entrada = null;
	DataOutputStream saida = null;
	Servidor copiaServidor = null;
	String novaMensagem;

	String nomeCliente = "";

	Bot bot = new Bot(this);

	// Construtor da classe
	public ClienteThread(Socket cliente, Servidor copia) {
		copiaServidor = copia;
		// Valida a referencia para o socket
		// Cria os canais de entrada e saída
		this.cliente = cliente;
		try {
			this.entrada = new DataInputStream(cliente.getInputStream());
			this.saida = new DataOutputStream(cliente.getOutputStream());
			this.start();
		} catch (Exception e) {
		}
	}

	public void enviaMensagem(String msg) {
		try {
			saida.writeUTF(msg);
			saida.flush();
		} catch (Exception e) {
		}
	}

	// Define o comportamento da Thread
	public void run() {
		while (true) {

			try {
				novaMensagem = entrada.readUTF();
				if (novaMensagem != null && !novaMensagem.equals("")) {
					copiaServidor.areaTexto.append(novaMensagem + "\n");
					bot.Responder(novaMensagem);
					Thread.sleep(400);
					bot.perguntar();
				}
			} catch (Exception e) {
			}
		}
	}

	public void respostaCliente() {
		enviaMensagem("************************************** Voc� ***********************************");
		enviaMensagem(novaMensagem);
	}
}
