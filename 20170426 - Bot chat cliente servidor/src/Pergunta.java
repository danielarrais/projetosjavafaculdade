import java.util.ArrayList;import java.util.Random;

import javax.xml.ws.ResponseWrapper;

public class Pergunta {
	String pergunta;
	String resposta;
	
	ArrayList<String> alternativas = new ArrayList<>();
	
	public Pergunta(String pergunta, String resposta, String[] alternativas){
		for (int i = 0; i < alternativas.length; i++) {
			this.alternativas.add(alternativas[i]);
		}
		this.pergunta = pergunta;
		this.resposta = resposta;
		Random r = new Random();
		this.alternativas.add(r.nextInt(3),resposta);
	}
	
	public String getPergunta(){
		String p = pergunta;
		int i = 1;
		for(String per:alternativas){
			
			p+="\n   "+i+")  "+per;
			i++;
		}
		return p;
		
	}
}
