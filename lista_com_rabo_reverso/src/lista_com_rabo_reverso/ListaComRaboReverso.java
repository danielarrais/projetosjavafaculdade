package lista_com_rabo_reverso;

public class ListaComRaboReverso<T> {
	No cabeca;
	No cauda;
	No temp = null;

	public void add(T dado) {
		No novo = new No(dado);
		if (cabeca == null) {
			cabeca = cauda = novo;
			return;
		}
		novo.setAnterior(cauda);
		cauda.setProximo(novo);
		
		cauda = novo;
	}

	public void ver() {
		temp = cabeca;
		for (No temp = cabeca; temp != null; temp = temp.getProximo()) {
			System.out.println(temp.getAnterior() + "<-" + temp.getDado() + " -> " + temp.getProximo());
		}
	}

	public void verInverso() {
		// temp = cauda;
		for (No temp = cauda; temp != null; temp = temp.getAnterior()) {
			System.out.println(temp.getAnterior() + " <- " + temp.getDado() + " -> " + temp.getProximo());
		}
	}

	public int size() {
		temp = cabeca;
		int i = 0;
		for (No temp = cabeca; temp != null; temp = temp.getProximo(), i++)
			;
		return i;
	}

	public T remove(int index) {
		if (index < 0 || index > size()) {
			throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
		}
		No excluido = null;
		if (index == 0) {
			excluido = cabeca;
			if (cabeca == cauda) {
				cabeca = cauda = null;
			} else if (cabeca.getProximo() == cauda) {
				cabeca = cauda;
			} else {
				cabeca.getProximo().setAnterior(null);
				cabeca = cabeca.getProximo();
			}
			return (T) excluido.getDado();
		}

		for (int j = 0; j < index - 1; j++, temp = temp.getProximo())
		excluido = temp.getProximo();
		if (temp.getProximo()==cauda) {
			cauda = temp;
			temp.setProximo(null);
			return (T) excluido;
		}
		temp.getProximo().getProximo().setAnterior(temp);
		temp.setProximo(temp.getProximo().getProximo());
		return (T) excluido;
	}

	public T busca(int index) {
		if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
        }
        temp = cabeca;
        for (int j = 0; j < index; j++, temp = temp.getProximo());
        return (T) temp.getDado();
	}

	public void add(int index, T dado) {
		if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException();
        }

        No<T> novo = new No<T>(dado);
        No<T> temp = cabeca;

        if (index == 0) {
            novo.setProximo(cabeca);
            cabeca = novo;
            cabeca.getProximo().setAnterior(cabeca);
            return;
        } else if (index == size()) {
        	novo.setAnterior(cauda);
            cauda.setProximo(novo);
            cauda = novo;
            return;
        }else{
        	if (index < size()/2 ) {
        		for (int j = 0; j < index - 1; j++, temp = temp.getProximo());
            	temp.getProximo().setAnterior(novo);
                novo.setProximo(temp.getProximo());
                novo.setAnterior(temp);
                temp.setProximo(novo);
			}else{
				for (int j = 0; j < index - 1; j++, temp = temp.getAnterior());
            	temp.getAnterior().setProximo(novo);//ant prox
                novo.setAnterior(temp.getAnterior());//ant ant
                novo.setProximo(temp);//prox
                temp.setProximo(novo);
			}
        	
        }
        
	}
}
