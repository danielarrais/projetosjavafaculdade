import lista_com_rabo_reverso.ListaComRaboReverso;

public class Main {
	public static void main(String... args) {
		ListaComRaboReverso<Integer> numeros = new ListaComRaboReverso<>();

		numeros.add(0);
		numeros.add(1);
		numeros.add(2);
		numeros.add(3);
		numeros.add(5);
		numeros.add(6);
		numeros.add(7);
		numeros.add(8);
		numeros.add(9);
		numeros.remove(8-1);
		numeros.remove(1);

		System.out.println("=========" + numeros.size() + "==========");
		numeros.ver();
		System.out.println("===================");
		numeros.verInverso();
		System.out.println("===================");
	}
}
