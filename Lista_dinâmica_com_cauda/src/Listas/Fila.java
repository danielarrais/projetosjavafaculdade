package Listas;


public class Fila<T> extends MinhaLinkedLista<T> {
    public void inserir(T conteudo) {
        super.add(super.size()-1, conteudo);
    }
    public T remover() {
        return (T) super.remove(0);
    }
}