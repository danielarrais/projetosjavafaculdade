package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controllers.CViewInput;

public class ViewTelaInput extends JFrame {
	private JTextField jtSegundos;
	private JTextField jtMinutos;
	private JTextField jtHoras;
	
	CViewInput controller = null;

	boolean pronto;

	private int minutos = 0, segundos = 0, horas = 0;

	JButton jbOk = null;

	public ViewTelaInput() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(420, 80);
		setTitle("Cronometro");
		setLayout(new FlowLayout());
		
		controller = new CViewInput(this);

		jtSegundos = new JTextField("00", 8);
		jtMinutos = new JTextField("00", 8);
		jtHoras = new JTextField("00", 8);

		jbOk = new JButton("Iniciar");
		jbOk.addActionListener(controller);

		add(jtHoras);
		add(new JLabel(" : "));
		add(jtMinutos);
		add(new JLabel(" : "));
		add(jtSegundos);

		add(jbOk);

		setVisible(true);
	}

	public JTextField getJtSegundos() {
		return jtSegundos;
	}

	public JTextField getJtMinutos() {
		return jtMinutos;
	}

	public JTextField getJtHoras() {
		return jtHoras;
	}

	public boolean isPronto() {
		return pronto;
	}

	public int getHoras() {
		return horas;
	}

	public int getMinutos() {
		return minutos;
	}

	public int getSegundos() {
		return segundos;
	}

	public JButton getJbOk() {
		return jbOk;
	}

	public void setPronto(boolean pronto) {
		this.pronto = pronto;
	}

	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}

	public void setSegundos(int segundos) {
		this.segundos = segundos;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

	public void TratarTempo() {
		if (minutos == 0 && segundos == 0 && horas > 1) {
			horas--;
			minutos = 59;
			segundos = 59;
		}
		if (horas == 1 && minutos == 0) {
			horas = 0;
			minutos = 59;
			segundos = 59;
		}
		if (minutos == 1) {
			minutos = 0;
			segundos = 59;
		}
		if (segundos == 1) {
			segundos = 0;
		}
	}
}
