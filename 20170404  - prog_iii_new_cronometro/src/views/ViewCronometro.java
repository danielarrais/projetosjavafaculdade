package views;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controllers.CViewCronometro;
import models.ModelCronometro;

public class ViewCronometro extends JFrame implements Runnable {
	JMenuBar menu = null;
	JMenu config = null;
	JMenuItem jbTempo = null;
	JMenuItem sair = null;

	CViewCronometro controller = null;

	ModelCronometro cronometro = null;

	JTextField jcTempo = null;

	URL url = null;
	AudioClip clip = null;

	JButton iniciar = null;
	JButton pausar = null;
	JButton zerar = null;

	Thread contar = null;

	JPanel opcoes = null;

	public ViewCronometro() throws MalformedURLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 200);
		setTitle("Cronometro");
		setLayout(new BorderLayout());
		setResizable(false);
		controller = new CViewCronometro(this);
		
		cronometro = new ModelCronometro();

		menu = new JMenuBar();
		setJMenuBar(menu);

		jbTempo = new JMenuItem("Tempo");
		jbTempo.addActionListener(controller);

		sair = new JMenuItem("Sair");
		sair.addActionListener(controller);

		config = new JMenu("Config");
		config.add(jbTempo);
		config.add(sair);
		menu.add(config);

		iniciar = new JButton("Iniciar");
		iniciar.addActionListener(controller);

		pausar = new JButton("Pausar");
		pausar.addActionListener(controller);

		zerar = new JButton("Zerar");
		zerar.addActionListener(controller);

		opcoes = new JPanel();
		opcoes.setLayout(new FlowLayout());

		opcoes.add(iniciar);
		opcoes.add(pausar);
		opcoes.add(zerar);

		add(opcoes, BorderLayout.SOUTH);

		setVisible(true);

		contar = new Thread(this);
		contar.start();
	}

	public void paint(Graphics g) {
		super.paint(g);
		if (cronometro.isPausado() && cronometro.getMinuto() == 0 && cronometro.getHora() == 0
				&& cronometro.getSegundo() < 11 && cronometro.getSegundo() > 0) {
			g.setColor(Color.red);
		} else {
			g.setColor(Color.black);
		}
		g.setFont(new Font("arial", Font.TRUETYPE_FONT, 40));
		g.drawString(cronometro.getHora() + " : " + cronometro.getMinuto() + " : " + cronometro.getSegundo() + " : "
				+ (cronometro.getMilisegundo()), 80, 100);

	}

	@Override
	public void run() {
		while (true) {
			System.out.println("");
			if (getCronometro().isPausado()==true) {
				cronometro.atualizarCronometro();
				repaint();
			}
		}
	}

	public JMenuItem getSair() {
		return sair;
	}

	public JButton getIniciar() {
		return iniciar;
	}

	public JButton getZerar() {
		return zerar;
	}

	public ModelCronometro getCronometro() {
		return cronometro;
	}

	public JButton getPausar() {
		return pausar;
	}

	public JMenuItem getJbTempo() {
		return jbTempo;
	}

}
