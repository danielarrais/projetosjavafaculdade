package models;

import javax.swing.JPanel;

public class ModelCronometro {

	boolean pausado;

	int hora = 0;
	int minuto = 0;
	int segundo = 0;
	long milisegundo = 0;
	long milisegundoInicial = 0;

	public void zerarTempo() {
		minuto = 0;
		hora = 0;
		segundo = 0;
		milisegundo = 0;
		milisegundoInicial = 0;
	};

	public void atualizarCronometro() {
		if (pausado == true) {
			if (minuto == 0 && hora == 0 && segundo <= 0) {
				zerarTempo();
				pausado = false;
			} else {
				setMilisegundo();
				// casos especiais
				tratarTempo();
				if (milisegundo >= 1000) {
					milisegundoInicial = System.currentTimeMillis();
					// tratar segundos
					if (segundo == 0) {
						if (minuto > 0) {
							segundo = 59;
						}
						tratarTempo();
						// tratar minutos
						if (minuto == 0) {
							if (hora > 0) {
								minuto = 59;
								hora--;
								tratarTempo();
							}
						} else {
							minuto--;
						}

					} else {
						segundo--;
					}
				}
			}
		}
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

	public int getSegundo() {
		return segundo;
	}

	public void setSegundo(int segundo) {
		this.segundo = segundo;
	}

	public long getMilisegundo() {
		return milisegundo;
	}

	public void setMilisegundo() {
		this.milisegundo = System.currentTimeMillis() - getMilisegundoInicial();
	}

	public long getMilisegundoInicial() {
		return milisegundoInicial;
	}

	public void setMilisegundoInicial() {
		this.milisegundoInicial = System.currentTimeMillis();
	}

	public boolean isPausado() {
		return pausado;
	}

	public void setPausado(boolean pausado) {
		this.pausado = pausado;
	}

	public void tratarTempo() {
		if (segundo == 1) {
			segundo = 0;
		} else if (minuto == 1 && hora > 0 && segundo == 0) {
			minuto = 0;
			segundo = 59;
		} else if (hora > 0 && minuto == 0 && segundo == 0) {
			hora--;
			minuto = 59;
			segundo = 59;
		} else if (minuto == 0 && segundo == 0 && hora > 0) {
			hora--;
			minuto = 59;
			segundo = 59;
		}

	}

}
