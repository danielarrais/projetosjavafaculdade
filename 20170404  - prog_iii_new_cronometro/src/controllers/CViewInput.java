package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import views.ViewTelaInput;

public class CViewInput implements ActionListener {
	ViewTelaInput tela = null;

	public CViewInput(ViewTelaInput tela) {
		this.tela = tela;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		tela.setMinutos(Integer.valueOf(tela.getJtMinutos().getText()));
		tela.setSegundos(Integer.valueOf(tela.getJtSegundos().getText()));
		tela.setHoras(Integer.valueOf(tela.getJtHoras().getText()));
		if (tela.getMinutos()>60) {
			tela.setHoras(tela.getHoras()+tela.getMinutos()/60);
			tela.setMinutos(tela.getMinutos()%60);
		}else if (tela.getSegundos()>60) {
			tela.setMinutos(tela.getMinutos()+tela.getSegundos()/60);
			tela.setSegundos(tela.getSegundos()%60);
			if (tela.getMinutos()>60) {
				tela.setHoras(tela.getHoras()+tela.getMinutos()/60);
				tela.setMinutos(tela.getMinutos()%60);
			}
		}
		tela.dispose();
		tela.setPronto(true);

	}

}
