package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import views.ViewTelaInput;
import views.ViewCronometro;

public class CViewCronometro implements ActionListener {
	ViewCronometro tela =null;
	
	public CViewCronometro(ViewCronometro tela) {
		this.tela = tela;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == tela.getSair()) {
			System.exit(1);
		} else if (e.getSource() == tela.getIniciar()) {
			tela.getCronometro().setPausado(true);
		} else if (e.getSource() == tela.getZerar()) {
			tela.getCronometro().zerarTempo();
			tela.repaint();
		} else if (e.getSource() == tela.getPausar()) {
			tela.getCronometro().setPausado(false);
		} else if (e.getSource() == tela.getJbTempo()) {
			ViewTelaInput entrada = new ViewTelaInput();
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						System.out.println("sadas");
						if (entrada.isPronto()) {
							tela.getCronometro().setMinuto(entrada.getMinutos());
							tela.getCronometro().setHora(entrada.getHoras());
							tela.getCronometro().setSegundo(entrada.getSegundos());
							tela.getCronometro().setMilisegundoInicial();
							tela.getCronometro().setPausado(true);
							break;
						}

					}

				}
			});
			thread.start();
		}
	}
}
