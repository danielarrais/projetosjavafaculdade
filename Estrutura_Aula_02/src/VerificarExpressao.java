import pilha.Pilha;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class VerificarExpressao extends JFrame implements ActionListener {

    JTextField campoExpresao = null;
    JButton validar = null;
    JLabel msg = null;
    JLabel rotulo = null;

    public VerificarExpressao() {
        setTitle("Validador de expressões");
        setSize(500, 500);
        setLocation(0, 0);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new FlowLayout((int) CENTER_ALIGNMENT));

        campoExpresao = new JTextField(40);
        validar = new JButton("Validar!");
        msg = new JLabel("Insira uma expressão no campo para ser validada.");
        rotulo = new JLabel("Expressão:");

        add(rotulo);
        add(campoExpresao);
        add(validar);
        add(msg);
        validar.addActionListener(this);
        setVisible(true);
    }

    public boolean validarExpressao(String expressao) {
        //Cria uma pilha que vai armazenar o colchetes e parênteses
        Pilha<String> expressaoValidar = new Pilha<>();
        //Tranforma a expressão informada em um array
        char[] arrayCaracteres = expressao.toCharArray();
        //Pecorre todo o array com a expressão
        for (int i = 0; i < arrayCaracteres.length; i++) {
            //Pega a  última posição da fila, como ela é o tamanho ta pilha, temos de subitrair 1, mas quando for 0, ela não subtrai
            int ultimaPosiçãoDaPilha = expressaoValidar.tamanhoDaLista() == 0 ? 0 : expressaoValidar.tamanhoDaLista() - 1;
            //pega a caractere atual da experessão
            String caractere = String.valueOf(arrayCaracteres[i]);
            //Verifica se é algum caractere de abertura
            if ("[".equals(caractere)) {
                //se for adiciona ele na pilha
                expressaoValidar.add(caractere);
            } else if ("(".equals(caractere)) {
                expressaoValidar.add(caractere);
                //verifica se é algum caractere de fechamento
            } else if ("{".equals(caractere)) {
                expressaoValidar.add(caractere);
                //verifica se é algum caractere de fechamento
            } else if ("]".equals(caractere)) {
                //se for verifica não adiciona, apenas verifica se o ultimo caractere inserido na pilha,
                //corresponde ao par de fechamento, se houver e removido o conteudo da ultima posição da pilha
                if ("[".equals(expressaoValidar.buscarElemento(ultimaPosiçãoDaPilha))) {
                    //Antes de remover
                    //verifica se a pilha é vasia, se estiver vasia, então quer dizer que não há o caractere de abertura,
                    // logo a expressão é inválida.
                    if (expressaoValidar.tamanhoDaLista() == 0) {
                        //e retorna false
                        return false;
                    } else {
                        //se houver, é removido a última posição da pilha
                        expressaoValidar.remover();
                    }
                }
            //o mesmo é feito com os outros caracteres
            } else if (")".equals(caractere)) {
                if ("(".equals(expressaoValidar.buscarElemento(ultimaPosiçãoDaPilha))) {
                    if (expressaoValidar.tamanhoDaLista() == 0) {
                        return false;
                    } else {
                        expressaoValidar.remover();
                    }
                }
            }else if ("}".equals(caractere)) {
                if ("{".equals(expressaoValidar.buscarElemento(ultimaPosiçãoDaPilha))) {
                    if (expressaoValidar.tamanhoDaLista() == 0) {
                        return false;
                    } else {
                        expressaoValidar.remover();
                    }
                }
            }
        }
        if (expressaoValidar.tamanhoDaLista() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        new VerificarExpressao();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean valido = validarExpressao(String.valueOf(campoExpresao.getText()));
        if (valido) {
            msg.setText("A expressão é válida!!!");
        } else {
            msg.setText("A expressão não é válida!!!");
        }
    }

}
