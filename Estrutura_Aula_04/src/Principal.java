
public class Principal {

    public static void main(String[] args) {
        Fila<Integer> minhaFila = new Fila<Integer>();

        Pilha<Integer> minhaPilha = new Pilha<Integer>();

        minhaFila.add(1);
        minhaFila.add(2);
        minhaFila.add(3);
        minhaFila.add(4);
        minhaFila.add(5);
        minhaFila.add(6);
        minhaFila.verLinkedList();
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());
        System.out.println(minhaFila.remover());        System.out.println(minhaFila.remover());

        System.out.println("=======================================");

        minhaPilha.add(1);
        minhaPilha.add(2);
        minhaPilha.add(3);
        minhaPilha.add(4);
        minhaPilha.add(5);
        minhaPilha.add(6);
        minhaPilha.verLinkedList();
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());
        System.out.println(minhaPilha.remover());


//        MinhaLinkedLista linkedList = new MinhaLinkedLista();
//
//        linkedList.add("OIA0");
//        linkedList.add("OIA1");
//        linkedList.add("OIA2");
//        linkedList.add("OIA3");
//        linkedList.add("OIA4");
//        linkedList.add("OIA5");
//
//        System.out.print("Buscar:");
//        System.out.println(linkedList.busca(5));
//        System.out.println(linkedList.size());
//
//        System.out.println("Removido: " + linkedList.remove(5));
//        System.out.println("--------------------:");
//        linkedList.add(5, "adicionado!");
//        linkedList.add(linkedList.size(), "adicionado!1");
//
//        System.out.println("--------------------:");
//        linkedList.verLinkedList();
    }

}
