
public class MinhaLinkedLista<T> {
    No cabeca;
    No temp = null;

    void add(T dado) {
        No novo = new No(dado);
        temp = cabeca;
        if (cabeca == null) {
            cabeca = novo;
            return;
        }
        while (temp.getProximo() != null) {
            temp = temp.getProximo();
        }
        temp.setProximo(novo);
    }

    void verLinkedList() {
        temp = cabeca;
        for (No temp = cabeca; temp != null; temp = temp.getProximo()) {
            System.out.println(temp.getDado() + " -> " + temp.getProximo());
        }
    }

    int size() {
        temp = cabeca;
        int i = 0;
        for (No temp = cabeca; temp != null; temp = temp.getProximo(), i++);
        return i;
    }

    public T remove(int index) {
        if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
        }
        temp = cabeca;
        No excluido = null;
        if (index == 0) {
            excluido = cabeca;
            cabeca = (cabeca.getProximo() == null) ? null : cabeca.getProximo();
            return (T) excluido.getDado();
        } else {
            for (int j = 0; j < index - 1; j++, temp = temp.getProximo()) ;
            excluido = temp.getProximo();
            temp.setProximo(temp.getProximo().getProximo());
        }
        return (T) excluido.getDado();
    }

    T busca(int index) {
        if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
        }
        temp = cabeca;
        for (int j = 0; j < index; j++, temp = temp.getProximo());
        return (T) temp.getDado();
    }

    void add(int index, T dado) {
        if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
        }
        temp = cabeca;
        No novo = new No(dado);
        if (index == 0) {
            novo.setProximo(temp);
            cabeca = novo;
        } else {
            for (int j = 0; j < index-1; j++, temp = temp.getProximo()) ;
            novo.setProximo(temp.getProximo());
            temp.setProximo(novo);
        }
    }
}
