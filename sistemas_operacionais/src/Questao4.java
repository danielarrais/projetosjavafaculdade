public class Questao4 {
	public static void main(String[] args) throws InterruptedException {
		int i = 100;

		String[] s1 = new String[i];

		// Thread 1 que recebe uma inta^ncia da classe Questao3
		// que implementa um runnable que cria os dois arrays
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					for (int i = 0; i < (s1.length / 2); i++) {
						System.out.println(s1[i] = "T1 preenchendo posi��o: " + (i));
					}
					// Encerra o while, logo encerra a Thread
					break;
				}

			}
		});

		// Thread 2 com Runnable() an�nimo, que vai fazer a soma dos dois arrays
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					for (int i = 49; i < (s1.length); i++) {
						System.out.println(s1[i] = "T2 preenchendo posi��o: " + (i));
					}
					// Encerra o while, logo encerra a Thread
					break;
				}
			}
		}

		);

		t1.start();
		t2.start();
	}
}
