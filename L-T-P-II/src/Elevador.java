
import java.util.Scanner;

public class Elevador {

    Scanner s = new Scanner(System.in);
    String Modelo;
    int Capacidade;
    int Andares;
    int Local = 0;
    boolean estadoPorta = true;
    boolean offOn = true;

    public Elevador(String Modelo, int Capacidade, int Andares) {
        this.Modelo = Modelo;
        this.Capacidade = Capacidade;
        this.Andares = Andares;
    }
    
    void EscolherAndar() {
        System.out.println("Informe o andar no qual você deseja ir!! ");
        irParaAndar(s.nextInt());
    }

    void Subir(int Andar) {
        if (estadoPorta) {
            Local = Andar;
        } else {
            estadoPorta = true;
            Local = Andar;
        }
    }

    void Descer(int Andar) {
        Local = Andar;
    }

    void irParaAndar(int Andar) {
        if (Andar < Andares) {
            if (Andar > Local) {
                Subir(Andar);
                System.out.println("O elevador subiu e estar no: " + Local + "° andar");
            } else {
                Descer(Andar);
                System.out.println("O elevador desceu e estar no: " + Local + "° andar");
            }
        } else {
            System.out.println("Andar informado não existe!!!");
        }
    }

    void Chamar(int Andar) {
        if (offOn) {
            if (Andar == Local) {
                estadoPorta = true;
                System.out.println("Entre no elevador!");
                EscolherAndar();
            } else {
                irParaAndar(Andar);
                System.out.println("Entre no elevador!");
                EscolherAndar();
            }
        } else {
            System.out.println("Elevador com Problemas ou desativado!!!"
                    + "Tente outro elevado ou vá pela escadaria!!");
        }
    }

    void offOn(boolean OnOff) {
        offOn = OnOff;
    }
}
