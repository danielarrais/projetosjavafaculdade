public class Pessoa {

    int Idade;
    String Nome;
    String Endereco;
    long CPF;

    public void mostraIdade() {
        System.out.println(Idade);
    }

    public boolean eMaior() {
        if (Idade >= 16) {
            return true;
        } else {
            return false;
        }
    }

    public void podeAssistir() {
        if (eMaior()) {
            System.out.println("Você pode assistir o filme!! ");
        } else {
            System.out.println("Você não pode assitir esse filme! pois você tem apenas " + Idade + " anos!");
        }
    }

    void inserirCPF(long CPF) {
        this.CPF = CPF;
        System.out.println("Você inseriu o CPF " + CPF);
    }

    void Envelhecer() {
        Idade++;
    }
}
