package Listas;

public class MinhaLinkedLista<T> {

    No cabeca;
    No cauda;
    No temp = null;

    public void add(T dado) {
        No novo = new No(dado);
        if (cabeca == null) {
            cabeca = cauda = novo;
            return;
        }
        cauda.setProximo(novo);
        cauda = novo;
    }

    public void verLinkedList() {
        temp = cabeca;
        for (No temp = cabeca; temp != null; temp = temp.getProximo()) {
            System.out.println(temp.getDado() + " -> " + temp.getProximo());
        }
    }

    public int size() {
        temp = cabeca;
        int i = 0;
        for (No temp = cabeca; temp != null; temp = temp.getProximo(), i++);
        return i;
    }

    public T remove(int index) {
        if (index < 0 || index > size()) {
            throw new ArrayIndexOutOfBoundsException("Indice Inválido!!!");
        }
        temp = cabeca;
        No excluido = null;
        if (index == 0) {
            excluido = cabeca;
            if (cabeca == cauda) {
                cabeca = cauda = null;
            } else if (cabeca.getProximo() == cauda) {
                cabeca = cauda;
            } else {
                cabeca.setDado(cauda);
            }
            return (T) excluido.getDado();
        } else {
            for (int j = 0; j < index - 1; j++, temp = temp.getProximo()) ;
            excluido = temp.getProximo();
            if (temp.getProximo() == cauda) {
                cauda = temp;
            } else {
                temp.setProximo(temp.getProximo().getProximo());
            }
        }
        return (T) excluido.getDado();
    }

    public T busca(int index) {
        if (index < 0 || index > size()) {
           return null;
        }
        temp = cabeca;
        for (int j = 0; j < index; j++, temp = temp.getProximo());
        return (T) temp.getDado();
    }

    public void add(int posicao, T dado) {

        if (posicao < 0 || posicao > size()) {
            throw new ArrayIndexOutOfBoundsException();
        }

        //Cria o No
        No<T> novo = new No<T>(dado);
        No<T> temp = cabeca;
        //Se insercao na posico zero
        if (posicao == 0) {
            novo.setProximo(cabeca);
            cabeca = novo;
            cauda = cabeca;
            return;
        } else if (posicao == size()) {
            cauda.setProximo(novo);
            cauda = novo;
            return;
        }else{
        	for (int j = 0; j < posicao - 1; j++, temp = temp.getProximo());
            novo.setProximo(temp.getProximo());
            temp.setProximo(novo);
        }
        
        //Insercao nas demais posicoes, 
        //Encontra o anterior
        
        
    }

}
