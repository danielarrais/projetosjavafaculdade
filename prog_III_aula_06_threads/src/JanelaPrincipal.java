
import java.awt.Color;
import java.awt.Font;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class JanelaPrincipal extends JFrame {

    public static JLabel vrRotulo = null;
    public static JButton vrOk = null;
    public static int temperatura = 0;

    public JanelaPrincipal() throws HeadlessException {
        setSize(800, 600);
        setTitle("Threads em execução");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());

        vrRotulo = new JLabel("Aguardando...");
        vrRotulo.setFont(new Font("Arial", Font.BOLD, 20));
        vrRotulo.setForeground(Color.red);
        add(vrRotulo);

        vrOk = new JButton("OK");
        add(vrOk);

        setVisible(true);

        Monitor monitor = new Monitor();
        monitor.start();
        Reator reator = new Reator();
        reator.start();
    }

    

}



