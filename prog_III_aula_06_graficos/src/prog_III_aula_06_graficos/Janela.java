package prog_III_aula_06_graficos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Janela extends JFrame implements MouseMotionListener {

	Socket cliente = null;
	DataOutputStream canalSaida = null;
	JPanel opcoes = null;
	JPanel tela = null;

	String[] listaCores = new String[] { "Vermelho", "Verde", "Preto", "Azul" };

	JComboBox<String> cores = null;
	JComboBox<String> tamanhos = null;
	int x = 0, y = 0;

	public Janela() throws HeadlessException, UnknownHostException, IOException {
		setSize(600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("CLiente Aguardando...");
		setLayout(new FlowLayout());

		opcoes = new JPanel(new FlowLayout());

		cores = new JComboBox<>(listaCores);
		tamanhos = new JComboBox<>();
		for (int i = 0; i < 100; i++) {
			tamanhos.addItem(i+"");
		}
		tamanhos.setSelectedIndex(10);
		add(tamanhos);
		opcoes.setBackground(Color.white);
		opcoes.add(cores);
		tela = new JPanel(new FlowLayout());
		tela.setSize(this.getWidth(), 600);
		this.setBackground(Color.white);
		addMouseMotionListener(this);
		setVisible(true);
		add(opcoes);
		add(tela);
		cliente = new Socket("192.168.43.139", 3067);
		canalSaida = new DataOutputStream(cliente.getOutputStream());
		setTitle("Conectado ao servidor");

	}

	public static void main(String args[]) throws HeadlessException, UnknownHostException, IOException {
		new Janela();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		try {
			canalSaida.writeUTF(e.getX() + ";" + e.getY() + ";" + listaCores[cores.getSelectedIndex()]+";"+tamanhos.getSelectedIndex());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

}
