public class Pessoa {
    private String Nome;
    private String Telefone;
    private String Endereço;
    private Carteira Carteira = null;

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    public String getEndereço() {
        return Endereço;
    }

    public void setEndereço(String Endereço) {
        this.Endereço = Endereço;
    }

    public Carteira getCarteira() {
        return Carteira;
    }

    public void TirarCarteira(int Numero) {
        this.Carteira = new Carteira(Numero);
    }

    public Pessoa(String Nome, String Telefone, String Endereço) {
        this.Nome = Nome;
        this.Telefone = Telefone;
        this.Endereço = Endereço;
    }
}
