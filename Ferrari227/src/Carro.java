
public class Carro {

    private String Marca;
    private int Ano;
    private String Cor;
    private String Modelo;

    private Motor Motor;
    private Placa Placa;
    private Pessoa Dono;

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public int getAno() {
        return Ano;
    }

    public void setAno(int Ano) {
        this.Ano = Ano;
    }

    public String getCor() {
        return Cor;
    }

    public void setCor(String Cor) {
        this.Cor = Cor;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public void getMotor() {
        System.out.println("Estado: " + Motor.isEstado());
        System.out.println("Cilindradas: " + Motor.getCilindradas());
        System.out.println("Velocidade Maxima: " + Motor.getVelocidadeMAX());
        System.out.println("Temperatura: " + Motor.getTemperatura() + "Cº");
        System.out.println("Marcha Atual: " + Motor.getMarcha());
    }

    public void getPlaca() {
        System.out.println("Numero: " + Placa.getNumero());
        System.out.println("Cidade: " + Placa.getCidade());
        System.out.println("Estado: " + Placa.getEstado());
    }

    public Pessoa getDono() {
        return Dono;
    }

    public Carro(String Marca, int Ano, String Cor, String Modelo) {
        setMarca(Marca);
        setAno(Ano);
        setCor(Cor);
        setModelo(Modelo);
    }

    public void InstalarMotor(int Cilindradas, int velocidadeMAX, boolean Cambio) {
        this.Motor = new Motor(Cilindradas, velocidadeMAX, Cambio);
    }

    public void associarPlaca(String Numero, String Cidade, String Estado) {
        this.Placa = new Placa(Numero, Cidade, Estado);
    }

    public void LigarDesligar() {
        Motor.LigarDesligar();
    }

    public void Acelerar(int Valor) {
        Motor.Acelerar(Valor);
    }

    public void Vender(Pessoa Dono) {
        this.Dono = Dono;
        System.out.println("Carro vendido para " + Dono.getNome());
    }

    public void Tranferir(Pessoa novoDono) {
        System.out.print("O carro que era de " + Dono.getNome());
        Dono = novoDono;
        System.out.println(" agora é de " + Dono.getNome());
    }
}
