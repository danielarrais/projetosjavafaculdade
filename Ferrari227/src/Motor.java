
public class Motor {

    private int Cilindradas;
    private float Temperatura = 0;
    private boolean Estado = false;
    private int velocidade;
    private int velocidadeMAX;
    private boolean CambioAutomatico;
    private int Marcha = 0;

    public int getVelocidadeMAX() {
        return velocidadeMAX;
    }

    public int getMarcha() {
        return Marcha;
    }

    public void setMarcha(int Marcha) {
        this.Marcha = Marcha;
    }

    public void setVelocidadeMAX(int velocidadeMAX) {
        this.velocidadeMAX = velocidadeMAX;
    }

    public int getCilindradas() {
        return Cilindradas;
    }

    public void setCilindradas(int Cilindradas) {
        this.Cilindradas = Cilindradas;
    }

    public float getTemperatura() {
        return Temperatura;
    }

    public void setTemperatura(int velocidade) {
        float temp = (float) (90.0 / velocidadeMAX) * velocidade;
        if (temp < 40.0f && isEstado()) {
            Temperatura = 50.0f;
        } else {
            Temperatura = temp;
        }
    }

    public boolean isEstado() {
        return Estado;
    }

    public void LigarDesligar() {
        if (Estado) {
            Estado = false;
            setTemperatura(0);
        } else {
            Estado = true;
        }
    }

    public int getVelocidade() {
        return velocidade;
    }

    public boolean isCambioAutomatico() {
        return CambioAutomatico;
    }

    public void setCambioAutomatico(boolean CambioAutomatico) {
        this.CambioAutomatico = CambioAutomatico;
    }

    public void Acelerar(int velocidade) {
        if (isEstado()&& velocidade <= velocidadeMAX) {
            this.velocidade = velocidade;
            setTemperatura(velocidade);
        }else{
            System.out.println("Ligue o carro para poder acelerar!!!");
        }

    }

    public void marchaAuto() {
        int vel = velocidadeMAX / 4;
        if (CambioAutomatico) {
            if (velocidade < vel) {
                setMarcha(1);
            } else if (velocidade > vel && velocidade <= (vel * 2)) {
                setMarcha(2);
            } else if (velocidade > vel * 2 && velocidade <= (vel * 3)) {
                setMarcha(3);
            } else if (velocidade > vel * 3 && velocidade <= (vel * 4)) {
                setMarcha(4);
            }
        }
    }

    public void ReduzirVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }

    public Motor(int Cilindradas, int velocidadeMAX, boolean Cambio) {
        setCilindradas(Cilindradas);
        setVelocidadeMAX(velocidadeMAX);
        setCambioAutomatico(Cambio);
    }

}
