public class Carteira {
    private int Numero;
    private int Ponto_R;
    final private int PONTOS_TOTAIS = 21;

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    public int getPonto_R() {
        return Ponto_R;
    }

    public void TirarPontos(int Pontos) {
        if (Pontos <= PONTOS_TOTAIS && Pontos > 0 ) {
            Ponto_R = PONTOS_TOTAIS - Pontos;
        }else if(Pontos > Ponto_R){
            System.out.println("Você Perdeu Sua Carteira!");
        }
    }

    public Carteira(int Numero) {
        this.Numero = Numero;
    }
    
    
}
