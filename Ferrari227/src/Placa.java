
public class Placa {

    private String Numero;
    private String Cidade;
    private String Estado;

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String Numero) {
        this.Numero = Numero;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String Cidade) {
        this.Cidade = Cidade;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Placa(String Numero, String Cidade, String Estado) {
        setCidade(Cidade);
        setNumero(Numero);
        setEstado(Estado);
    }

}
