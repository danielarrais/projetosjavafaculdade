import java.util.Scanner;
public class Atv5_While {
        public static void main(String[] args) {
                Scanner s = new Scanner(System.in);
                float   imc1 = 0,
                        imc2 = 0,
                        imc3 = 0,
                        imc4 = 0,
                        imc5 = 0,
                        imc6 = 0,
                        imc7 = 0,
                        imc8 = 0,
                        imc9 = 0,
                        imc10 = 0;
                int i = 1;
                System.out.println("----------------------------------------");
                System.out.println(" ----------------Valores----------------");
                System.out.println("----------------------------------------");
                while (i <= 10) {
                        float p, a;
                        System.out.print("Peso da pessoa "+i+": ");
                        p = s.nextFloat();
                        System.out.print("Altura da pessoa "+i+": ");
                        a = s.nextFloat();
                        if (i==1) {imc1=p/(a*a);}
                        if (i==2) {imc2=p/(a*a);}
                        if (i==3) {imc3=p/(a*a);}
                        if (i==4) {imc4=p/(a*a);}
                        if (i==5) {imc5=p/(a*a);}
                        if (i==6) {imc6=p/(a*a);}
                        if (i==7) {imc7=p/(a*a);}
                        if (i==8) {imc8=p/(a*a);}
                        if (i==9) {imc9=p/(a*a);}
                        if (i==10) {imc10=p/(a*a);}
                        i++;
                }
                System.out.println("----------------------------------------");
                System.out.println("-----------------IMC's-----------------");
                System.out.println("----------------------------------------");
                while (i <= 10)  {
                    if (i==1) {System.out.println("IMC da pessoa "+i+": "+imc1);}
                    if (i==2) {System.out.println("IMC da pessoa "+i+": "+imc2);}
                    if (i==3) {System.out.println("IMC da pessoa "+i+": "+imc3);}
                    if (i==4) {System.out.println("IMC da pessoa "+i+": "+imc4);}
                    if (i==5) {System.out.println("IMC da pessoa "+i+": "+imc5);}
                    if (i==6) {System.out.println("IMC da pessoa "+i+": "+imc6);}
                    if (i==7) {System.out.println("IMC da pessoa "+i+": "+imc7);}
                    if (i==8) {System.out.println("IMC da pessoa "+i+": "+imc8);}
                    if (i==9) {System.out.println("IMC da pessoa "+i+": "+imc9);}
                    if (i==10) {System.out.println("IMC da pessoa "+i+": "+imc10);}
                    i++;
                }
        }
}
