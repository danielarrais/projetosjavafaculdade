
import java.util.Scanner;

public class Atv2_Do_While {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int Value;
        int i = 1;
        System.out.print("Informe um valor qualquer maior que 1: ");
        Value = s.nextInt();
        do {
            System.out.println(i);
            i++;
        }while ( i <= Value);
    }
}
