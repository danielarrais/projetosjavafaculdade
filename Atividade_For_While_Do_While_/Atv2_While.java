
import java.util.Scanner;

public class Atv2_While {
     public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int v;
        int i = 1;
        System.out.print("Informe um valor qualquer maior que 1: ");
        v = s.nextInt();
        while ( i <= v) {
            System.out.println(i);
            i++;
        }
    }
}
