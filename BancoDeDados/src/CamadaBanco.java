import java.sql.*;
import java.util.Scanner;

public class CamadaBanco {
	Connection conexao = null;
	String sql = null;

	public CamadaBanco() throws SQLException {
		try {
			conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/filaesperta?useSSL=false", "root", "root");
			String sql = "SELECT ?, ? FROM professores INNER JOIN professoresUniversidades where nome = ? and sigla = ?;";
			PreparedStatement consulta = conexao.prepareStatement(sql);
			consulta.setString(1, "nome");
			consulta.setString(2, "sigla");
			consulta.setString(3, "silvano");
			consulta.setString(4, "UNITINS");
			ResultSet resultado = consulta.executeQuery();
			ResultSetMetaData metaDados = resultado.getMetaData();

			int numeroColunas = metaDados.getColumnCount();
			while (resultado.next()) {
				for (int i = 1; i <= numeroColunas; i++) {
					System.out.println(metaDados.getColumnName(i) + ": " + resultado.getObject(i));
				}
				System.out.println();
			}

			resultado.close();
			consulta.close();
			conexao.close();
		} catch (Exception e) {
			System.out.println("Deu PAU!!");
		}
	}

	public void inserirProfessor(String professor, String faculdade) throws SQLException {
		conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/academica?useSSL=false", "root", "root");
		String sql = "insert into professores(nome, sobrenome) values (?, ?);";
		PreparedStatement consulta = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		consulta.setString(1, professor);
		consulta.setString(2, "dsdsdfsdf");
		consulta.execute();
		ResultSet inseridos = consulta.getGeneratedKeys();

		sql = "SELECT nome FROM professores where nome = ?;";
		consulta = conexao.prepareStatement(sql);
		consulta.setString(1, professor);
		ResultSet r = consulta.executeQuery();
		int id_professor = 0;
		if (!r.next()) {
			id_professor = r.getInt("nome");
		}

		sql = "SELECT nome FROM universidades where nome = ?;";
		consulta = conexao.prepareStatement(sql);
		consulta.setString(1, "nome");
		r = consulta.executeQuery();

		String sigla = null;
		if (!r.next()) {
			Scanner s = new Scanner(System.in);

			System.out.println("Faculdade n�o cadastrada!!! por favor, a qual estado essa univerdade pertence?");
			String estado = s.next();
			System.out.println("Agora informe a sigla:");
			sigla = s.next();

			sql = "insert into universidades(sigla, nome, estado) values (?,?,?);";
			consulta = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			consulta.setString(1, sigla);
			consulta.setString(2, faculdade);
			consulta.setString(3, estado);
			consulta.execute();

		}
		
		int id_universidade = 0;
		if (r.next()) {
			id_universidade =inseridos.getInt(1);
		}
		
		sql = "insert into professoresuniversidades(idProfessor, sigla) values (?,?);";
		consulta = conexao.prepareStatement(sql);
		
		consulta.setInt(1, id_professor);
		consulta.setString(2, sigla);
		consulta.execute();

		r.close();
		consulta.close();
		conexao.close();
	}
}
