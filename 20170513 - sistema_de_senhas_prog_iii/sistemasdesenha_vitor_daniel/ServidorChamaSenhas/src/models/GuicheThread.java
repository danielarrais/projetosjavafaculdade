package models;

import java.io.*;
import java.net.Socket;

import views.Servidor;

public class GuicheThread extends Thread {
	// Atributos da classe
	public Socket cliente = null;
	public DataInputStream entrada = null;
	DataOutputStream saida = null;
	Servidor copiaServidor = null;

	String id_thread = null;
	Guiche g = null;

	// Construtor da classe
	public GuicheThread(Socket cliente, Servidor copia) {
		copiaServidor = copia;
		this.cliente = cliente;
		try {
			this.entrada = new DataInputStream(cliente.getInputStream());
			this.saida = new DataOutputStream(cliente.getOutputStream());
			this.start();
		} catch (Exception e) {
		}
	}

	// envia mensagem
	public void enviaMensagem(String msg) {
		try {
			saida.writeUTF(msg);
			saida.flush();
		} catch (Exception e) {
		}
	}

	// Define o comportamento da Thread
	public void run() {
		while (true) {
			String msg = null;
			try {
				msg = entrada.readUTF();
			} catch (IOException e) {
			}

			// verifica se a mensagem n�o � nula
			if (msg != null) {

				// separa cabe�alho da mensagem dos dados
				String[] dados = msg.split(":");

				switch (dados[0]) {
				// verificao conteudo do cabe�alho e executar algo de acordo com
				// cabe�alho recebido
				case "IDGUICHE":
					try {
						copiaServidor.crud.cadastrarGuiche(g);
						try {
							copiaServidor.crud.abrirGuiche(g);
						} catch (Exception e) {
						}
					} catch (Exception e) {
						copiaServidor.crud.abrirGuiche(g);
					}
					break;
				case "CHAMAR":
					if (dados[1].equalsIgnoreCase("SEM")) {
					} else {
						dados = dados[1].split(";");
						copiaServidor.crud.chamarSenha(new Senha(dados[1], Integer.valueOf(dados[2])),
								new Guiche(Integer.valueOf(dados[0])));
					}
					copiaServidor.atualizaOrdem();
					if (copiaServidor.proximaCategoria.equals(dados[1])) {
						copiaServidor.atualizaOrdem();
					}
					break;
				default:
					break;
				}
			}

		}
	}
}
