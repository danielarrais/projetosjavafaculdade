package Views;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Crontrollers.CtrlGuiche;
import Listas.Fila;
import models.Senha;

public class PainelGuiche extends JFrame implements Runnable {

	private static final long serialVersionUID = 1L;

	public Thread thread = new Thread(this);

	String ultima = "";

	private JPanel paneGuiche;
	private JTable tableProximas;
	private JButton btnChamar, btnProxima;
	private JScrollPane scrollPaneTabela;
	private JLabel lblGuiche, lblProximasSenhas;

	// Socket para conectar ao servidor
	Socket PainelGuiche = null;

	// canais para comunica��o do servidor
	public DataInputStream canalEntrada = null;
	public DataOutputStream canalSaida = null;

	// variavel que armazena categoria a ser chamada
	String proximaCategoria = "";

	// variavel para controle do total de senhas existentes
	public Integer totalSenha = 0;

	// intancia da janela de captura do numero do guiche
	public PainelAberturaGuiche painelAbertura = null;

	// intancia de controle dos bot�es
	CtrlGuiche controller = new CtrlGuiche(this);

	// matriz que armazena os dados para serem utilizados na tabela
	String[][] matriz;

	Fila<String> p = null;
	Fila<String> c = null;
	Fila<String> j = null;

	public Integer idGuiche;
	public String idThread;

	// contrutor da classe
	public PainelGuiche() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 441);
		setResizable(false);
		paneGuiche = new JPanel();
		paneGuiche.setBackground(new Color(13, 82, 149));
		paneGuiche.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(paneGuiche);
		paneGuiche.setLayout(null);

		btnChamar = new JButton("Chamar");
		btnChamar.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnChamar.setForeground(Color.WHITE);
		btnChamar.addActionListener(controller);
		btnChamar.setBackground(new Color(255, 140, 0));
		btnChamar.setBounds(32, 345, 115, 33);
		paneGuiche.add(btnChamar);

		btnProxima = new JButton("Pr\u00F3xima");
		btnProxima.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnProxima.setForeground(Color.WHITE);
		btnProxima.setBackground(new Color(255, 140, 0));
		btnProxima.addActionListener(controller);
		btnProxima.setBounds(156, 345, 106, 33);
		paneGuiche.add(btnProxima);

		scrollPaneTabela = new JScrollPane();
		scrollPaneTabela.setBounds(32, 104, 230, 230);
		scrollPaneTabela.setBackground(new Color(13, 82, 149));
		paneGuiche.add(scrollPaneTabela);

		tableProximas = new JTable();
		tableProximas.setRowSelectionAllowed(false);
		tableProximas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneTabela.setViewportView(tableProximas);

		lblGuiche = new JLabel("");
		lblGuiche.setHorizontalAlignment(SwingConstants.CENTER);
		lblGuiche.setForeground(Color.WHITE);
		lblGuiche.setFont(new Font("Trebuchet MS", Font.BOLD, 27));
		lblGuiche.setBounds(0, 0, 284, 70);
		paneGuiche.add(lblGuiche);

		lblProximasSenhas = new JLabel("Pr\u00F3ximas Senhas");
		lblProximasSenhas.setHorizontalAlignment(SwingConstants.CENTER);
		lblProximasSenhas.setForeground(Color.WHITE);
		lblProximasSenhas.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblProximasSenhas.setBounds(0, 57, 284, 36);
		paneGuiche.add(lblProximasSenhas);
		Random s = new Random(1000);

		// Cria o objeto servidor com a porta
		// Valida os canais a partir do socket PainelGuiche
		try {
			PainelGuiche = new Socket("localhost", 3088);

			// intancia os canais de entrada e sa�da de dados
			canalEntrada = new DataInputStream(PainelGuiche.getInputStream());
			canalSaida = new DataOutputStream(PainelGuiche.getOutputStream());

			setTitle("Conectado ao servidor!");
		} catch (Exception e) {
		}

		// cria janela de captura do numero do guiche
		painelAbertura = new PainelAberturaGuiche();
		painelAbertura.setVisible(true);

		// aguarda um valor se digitado
		while (painelAbertura.valorID == null) {
			System.out.print("");
			if (painelAbertura.valorID != null) {
				idGuiche = painelAbertura.valorID.intValue();
				lblGuiche.setText("Guich� " + idGuiche);
				try {
					canalSaida.writeUTF("IDGUICHE:"+idGuiche);
					canalSaida.flush();
				} catch (IOException e) {
				}
				thread.start();
				painelAbertura.dispose();
				break;
			}
		}

		// abre o guiche
		setVisible(true);
	}

	public void run() {
		String msg = "";
		// Protocolo:
		// SENHAS:mensagem-CATEGORIA:mensagem-SC:mensagem-SNC:mensagem
		msg = "";
		while (true) {
			msg = "";

			// captura a mensagem
			try {
				msg = canalEntrada.readUTF();
			} catch (IOException e1) {
			}

			// File file = new File("//img//1.jpg");

			// verificação de cabeçalho
			String[] pacote = msg.split("-");

			// separa cabe�alho do pacote
			String[] categoria = pacote[0].split(":");
			String[] senha = pacote[2].split(":");
			totalSenha = senha.length - 1;
			// verifica se o pacote chegou e se h� senhas no pacote
			if (senha[0].equals("SNC") && totalSenha > 0) {
				// incializa as filas
				p = new Fila<>();
				c = new Fila<>();
				j = new Fila<>();

				// pega os dados(senhas) recebidos no pacote
				String dados = senha[1];

				// explode as strings e coloca elas em suas pespectivas filas
				String[] senhas = dados.split(" ");
				for (int i = 0; i < senhas.length; i++) {
					if (senhas[i].length() > 0) {
						String tipo = senhas[i].substring(0, 1);
						if (tipo.equalsIgnoreCase("P")) {
							p.inserir(senhas[i]);
						} else if (tipo.equalsIgnoreCase("C")) {
							c.inserir(senhas[i]);
						} else if (tipo.equalsIgnoreCase("J")) {
							j.inserir(senhas[i]);
						}
					}
				}

				// ver qual fila é maior para determinar o numero de linhas da
				// tabela
				int filaMaior = 0;
				if (p.size() > filaMaior) {
					filaMaior = p.size();
				}
				if (c.size() > filaMaior) {
					filaMaior = c.size();
				}
				if (j.size() > filaMaior) {
					filaMaior = j.size();
				}

				// controle validador
				if (totalSenha > 0) {
					this.getLblProximasSenhas().setText("Pr�ximas Senhas");
				} else {
					this.getLblProximasSenhas().setText("Sem senhas!!!");
				}

				// criação da matriz para tabela
				matriz = new String[filaMaior][3];
				for (int k = 0; k < filaMaior; k++) {
					matriz[k] = new String[] { c.busca(k), p.busca(k), j.busca(k) };
				}

				// armazena qual categoria (dado enviado pelo servidor) deve ser
				// chamada
				if (categoria.length > 1) {
					proximaCategoria = categoria[1];
				}

			} else {
				matriz = new String[0][0];
			}
			// atualiza a tabela
			atualizarTabela();
		}
	}

	// metodo que trar� a senha removida para o controle do painelGuiche
	public Senha proximaSenha() {
		Senha s = null;

		// verifica se h� senhas
		if (totalSenha > 0) {
			// ver qual � a proxima senha que deve ser removida, e a remove de
			// sua respectiva fila
			if (proximaCategoria.equalsIgnoreCase("P") && p.size() > 0) {
				Object senhaO = p.remover();
				String senha = String.valueOf(senhaO);
				s = new Senha(senha.substring(0, 1), Integer.valueOf(senha.replace("P", "")));
				return s;
			} else if (proximaCategoria.equalsIgnoreCase("C") && c.size() > 0) {
				Object senhaO = c.remover();
				String senha = String.valueOf(senhaO);
				s = new Senha(senha.substring(0, 1), Integer.valueOf(senha.replace("C", "")));
				return s;
			} else if (proximaCategoria.equalsIgnoreCase("J") && j.size() > 0) {
				Object senhaO = j.remover();
				String senha = String.valueOf(senhaO);
				s = new Senha(senha.substring(0, 1), Integer.valueOf(senha.replace("J", "")));
				return s;
			}
		}
		return s;
	}

	// metodo que coloca od dados da matriz na tabela e atualiza a tabela
	public void atualizarTabela() {

		// remove o scrooll da tabela
		this.remove(getScrollPaneTabela());

		// cria uma tabela com os dados atualizados
		JTable tabela;
		tabela = new JTable(matriz, new String[] { "C", "P", "J" });

		// cria um novo scrooll com a nova tabela, e a adciona na mesma posi��o
		// da antiga
		JScrollPane scrool = new JScrollPane(tabela);
		scrool.setBounds(32, 104, 230, 230);
		this.add(scrool);
	}

	// m�todo que incialisa o servidor
	public static void main(String[] args) {
		new PainelGuiche();
	}

	// get's e set's
	public JTable getTableProximas() {
		return tableProximas;
	}

	public JButton getBtnChamar() {
		return btnChamar;
	}

	public JButton getBtnProxima() {
		return btnProxima;
	}

	public JScrollPane getScrollPaneTabela() {
		return scrollPaneTabela;
	}

	public JLabel getLblGuiche() {
		return lblGuiche;
	}

	public JLabel getLblProximasSenhas() {
		return lblProximasSenhas;
	}

}
