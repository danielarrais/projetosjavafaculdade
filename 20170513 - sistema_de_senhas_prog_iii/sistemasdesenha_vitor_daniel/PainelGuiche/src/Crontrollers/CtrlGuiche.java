package Crontrollers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import Views.PainelAberturaGuiche;
import Views.PainelGuiche;
import models.Senha;

public class CtrlGuiche implements ActionListener {
	PainelGuiche painelGuiche;
	
	//contrutor do controlador
	public CtrlGuiche(PainelGuiche tela) {
		painelGuiche = tela;
	}

	@Override
	//sobrecrita do metodo actionPerformed da classe actionlinestter
	public void actionPerformed(ActionEvent e) {
		//verificar se o but�o pressionado foi o de proxima senha
		if (e.getSource() == painelGuiche.getBtnProxima()) {
			//verifica se h� senhas no painel
			if (painelGuiche.totalSenha > 0) {
				try {
					//captura a senha e a remove do painel
					Object o = painelGuiche.proximaSenha();
					Senha senha = (Senha) o;
					//verifica se a senha que veio n�o � nulla, se n�o for ela envia a senha juntamente com cabe�alho CHAMAR
					if (senha != null) {
						painelGuiche.canalSaida.writeUTF("CHAMAR:" + painelGuiche.idGuiche + ";" + senha.getCategoria()
								+ ";" + senha.getSenha());
						painelGuiche.canalSaida.flush();
					}
				} catch (IOException e1) {
				}
			} else {
				//caso n�o haja ele mostra ao usur�rio
				painelGuiche.getLblProximasSenhas().setText("Sem senhas!!!");
			}

		}
	}

}
