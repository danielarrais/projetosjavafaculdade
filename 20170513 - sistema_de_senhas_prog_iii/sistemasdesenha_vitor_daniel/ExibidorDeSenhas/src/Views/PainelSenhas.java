package Views;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.LocalDateTime;
import Listas.LinkedList;

import models.Senha;

public class PainelSenhas extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;
	
	//componnetes diversos de interface
	private JPanel contentPane;
	private JLabel lblSenhaAtual, lblSenhaPainel, lblGuichePainel, lblHorario, lblData, lblUtimasSenhas, lblSenha1,
			lblSenha2, lblSenha3, lblNewLabel;
	private JPanel panelSenhas, panel_1, panel_2, panelPublicidade;
	
	//thread para atualiza��o da data e hora do painel
	private Thread hora = null;
	
	//Socket para conex�o com o server
	Socket PainelSenhas = null;
	
	//canais de entrada e saida para comunica��o com o server
	DataInputStream canalEntrada = null;
	DataOutputStream canalSaida = null;
	
	//metodo run(), que deve ser executado pela Thread
	public void run() {
		
		while (true) {
			
			//arrays para armazena cabe�alho e dados
			String[] dados;
			String[] senha;

			//lista para armazenar senhas
			LinkedList<String[]> senhas = null;
			
			//vari�vel para armazenar a mensagem enviada pelo servidor
			String msg = "";
			try {
				//Protocolo:  SENHAS:mensagem-CATEGORIA:mensagem-SC:mensagem-SC:mensagem
				msg = canalEntrada.readUTF();
				System.out.println(msg);
				//intancia��o da lista
				senhas = new LinkedList<>();
				
				//separa��o dos pacotes
				String[] pacote = msg.split("-");
				
				//
				dados = pacote[1].split(":")[1].split(" ");

				//
				for (int i = 0; i < dados.length; i++) {
					senha = dados[i].split(";");
					senhas.add(senha);
				}
				
				if (senhas.busca(senhas.size() - 1) != null) {
					lblSenhaPainel.setText(senhas.busca(senhas.size() - 1)[0]);
					lblGuichePainel.setText("Guich� " + senhas.busca(senhas.size() - 1)[1]);
				}
				if (senhas.busca(senhas.size() - 2) != null) {
					lblSenha1.setText(
							senhas.busca(senhas.size() - 2)[0] + " - Guich� " + senhas.busca(senhas.size() - 2)[1]);
				}
				
				if (senhas.busca(senhas.size() - 3) != null) {
					lblSenha2.setText(
							senhas.busca(senhas.size() - 3)[0] + " - Guich� " + senhas.busca(senhas.size() - 3)[1]);
				} 
				if (senhas.busca(senhas.size() - 4) != null) {
					lblSenha3.setText(
							senhas.busca(senhas.size() - 4)[0] + " - Guich� " + senhas.busca(senhas.size() - 4)[1]);
				}
			} catch (Exception e) {
			}
		}
	}

	public static void main(String[] args) {
		new PainelSenhas();
	}

	public PainelSenhas() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(13, 82, 149));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelSenhas = new JPanel();
		panelSenhas.setBorder(null);
		panelSenhas.setBackground(new Color(22, 85, 204));
		panelSenhas.setBounds(836, 0, 370, 368);
		contentPane.add(panelSenhas);
		panelSenhas.setLayout(null);

		lblSenhaAtual = new JLabel("Senha Atual");
		lblSenhaAtual.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenhaAtual.setForeground(Color.WHITE);
		lblSenhaAtual.setFont(new Font("Arial", Font.BOLD, 34));
		lblSenhaAtual.setBounds(0, 40, 370, 88);
		panelSenhas.add(lblSenhaAtual);

		lblSenhaPainel = new JLabel("");
		lblSenhaPainel.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenhaPainel.setForeground(Color.WHITE);
		lblSenhaPainel.setFont(new Font("Arial", Font.BOLD, 99));
		lblSenhaPainel.setBounds(0, 126, 370, 88);
		panelSenhas.add(lblSenhaPainel);

		lblGuichePainel = new JLabel("");
		lblGuichePainel.setHorizontalAlignment(SwingConstants.CENTER);
		lblGuichePainel.setForeground(Color.WHITE);
		lblGuichePainel.setFont(new Font("Arial", Font.BOLD, 40));
		lblGuichePainel.setBounds(0, 225, 370, 88);
		panelSenhas.add(lblGuichePainel);

		panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBackground(new Color(22, 85, 204));
		panel_1.setBounds(1036, 645, 170, 46);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		lblHorario = new JLabel("22:22:22");
		lblHorario.setHorizontalAlignment(SwingConstants.CENTER);
		lblHorario.setForeground(Color.WHITE);
		lblHorario.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblHorario.setBounds(0, 0, 170, 46);
		panel_1.add(lblHorario);

		panel_2 = new JPanel();
		panel_2.setBorder(null);
		panel_2.setBackground(new Color(22, 85, 204));
		panel_2.setBounds(836, 645, 170, 46);
		contentPane.add(panel_2);
		panel_2.setLayout(null);

		lblData = new JLabel("05/02/2017");
		lblData.setForeground(Color.WHITE);
		lblData.setHorizontalAlignment(SwingConstants.CENTER);
		lblData.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblData.setBounds(0, 0, 170, 46);
		panel_2.add(lblData);

		panelPublicidade = new JPanel();
		panelPublicidade.setBounds(46, 44, 726, 603);
		ImageIcon i = new ImageIcon("C:\\Users\\danie\\Desktop\\Vitor_Sales__Daniel_arrais\\ExibidorDeSenhas\\src\\img\\1.jpg");
		lblNewLabel = new JLabel(i);
		lblNewLabel.setBounds(0, 0, 726, 603);
		panelPublicidade.add(lblNewLabel);
		contentPane.add(panelPublicidade);

		lblUtimasSenhas = new JLabel("\u00DAltimas senhas chamadas");
		lblUtimasSenhas.setHorizontalAlignment(SwingConstants.CENTER);
		lblUtimasSenhas.setForeground(Color.WHITE);
		lblUtimasSenhas.setFont(new Font("Trebuchet MS", Font.BOLD, 27));
		lblUtimasSenhas.setBounds(836, 413, 370, 70);
		contentPane.add(lblUtimasSenhas);

		lblSenha1 = new JLabel("");
		lblSenha1.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha1.setForeground(Color.WHITE);
		lblSenha1.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblSenha1.setBounds(836, 481, 370, 36);
		contentPane.add(lblSenha1);

		lblSenha2 = new JLabel("");
		lblSenha2.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha2.setForeground(Color.WHITE);
		lblSenha2.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblSenha2.setBounds(836, 516, 370, 36);
		contentPane.add(lblSenha2);

		lblSenha3 = new JLabel("");
		lblSenha3.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha3.setForeground(Color.WHITE);
		lblSenha3.setFont(new Font("Trebuchet MS", Font.BOLD, 25));
		lblSenha3.setBounds(836, 549, 370, 36);
		contentPane.add(lblSenha3);

		// atualiza a hora e data do monitor
		hora = new Thread(new Runnable() {
			public void run() {
				while (true) {
					// instancia da classe "locaDateTime" para definir a hora e
					// data
					LocalDateTime c = LocalDateTime.now();
					lblHorario.setText(
							addZero(c.getHour()) + ":" + addZero(c.getMinute()) + ":" + addZero(c.getSecond()));
					lblData.setText(addZero(c.getDayOfMonth()) + "/" + addZero(c.getMonth().getValue()) + "/"
							+ addZero(c.getYear()));
					c = null;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});
		hora.start();
		setVisible(true);

		try {
			PainelSenhas = new Socket("localhost", 3088);
			canalEntrada = new DataInputStream(PainelSenhas.getInputStream());
			canalSaida = new DataOutputStream(PainelSenhas.getOutputStream());
			new Thread(this).start();
			setTitle("Conectado ao servidor!");
		} catch (Exception e) {
		}
	}

	// add um zero caso os dados sejam menores que 10
	public String addZero(int value) {
		String valueC = "" + (value < 10 ? 0 : "") + value;
		return valueC;
	}
}
