
public class Quarto {

    public final static int QUARTO_SOLTEIRO = 0;
    public final static int QUARTO_CASAL = 1;

    int tipo;
    int numero;
    boolean arCondicionado;
    Hospede vrHospede;

    public int getTipo() {
        return tipo;
    }
    
    public boolean isOcupado()	{
        if (vrHospede == null) {
            return false;
        }else{
            return true;
        }
    }
    
    public int getNumero() {
        return numero;
    }

    public boolean isArCondicionado() {
        return arCondicionado;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setArCondicionado(boolean arCondicionado) {
        this.arCondicionado = arCondicionado;
    }

    public void setOcupado(Hospede Hospede) {
        this.vrHospede = Hospede;
    }
    
    public Quarto( int NumerodoQuarto, int tipo, boolean arCondicionado) {
        setNumero(NumerodoQuarto);
        setTipo(tipo);
        setArCondicionado(arCondicionado);
        this.vrHospede = null;
    }
    
    
}
