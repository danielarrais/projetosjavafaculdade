
public class Pizza extends Lanche{

	public static final int G = 2, M = 1, P = 0; 
	
	//Atributos da classe
	int tamanho;
	int totalIngredientes;
	Pizza(int tamanho, int totalIngredientes){
		this.tamanho = tamanho;
		this.totalIngredientes = totalIngredientes;
	}
	@Override
	void modoPreparo() {
	
		System.out.println("Pizza: Preparar a massa, "
				+"Colocar ingredientes"
				+"levar ao forno.");
		
	}
	@Override
	float precoProduto() {
		
		return tamanho * 20 + (totalIngredientes * 15);
	}

	
}
