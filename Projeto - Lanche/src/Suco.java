
public class Suco extends Bebida {

public static final int POLPA = 0, CREME = 1; 
	int tipo;
	
	Suco(int tipo){
		this.tipo = tipo;
		
	}

	@Override
	void tipoVasilha() {
		System.out.println("Copo Pl�stico");
	}

	@Override
	float precoProduto() {
		
		return tipo*10+10;
	}
	
	public String toString(){
		return nome + " " + precoProduto() + ""
				+((tipo == POLPA)? "POLPA": "CREME");
	}
}
