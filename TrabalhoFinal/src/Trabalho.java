import java.util.Random;
import java.util.Scanner;

public class Trabalho {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Random aleatorio = new Random();
        //Variável do menu
        int opcao = 0;
        //Vetor e matriz
	int[] vector = new int[10];
	int[][] tabela = new int[4][5];
        //Variáveis para verificar se o vetor e a matriz estão preenchido
        boolean vetorPreenchido = false, matrizPreenchida = false;
        //Variáveis para uso na ordenação do vetor e da matriz
        int auxOrdenacao = 0, k = 0;
        int[] vetorAuxDeOrdDaMatriz = new int[20];

        while (opcao != 7) {
            System.out.println("Menu:");
            System.out.println("1 - Preencher vetor");
            System.out.println("2 - Ordenar vetor");
            System.out.println("3 - Imprimir vetor");
            System.out.println("4 - Preencher matriz");
            System.out.println("5 - Ordenar matriz");
            System.out.println("6 - Imprimir matriz");
            System.out.println("7 - Sair");
            opcao = s.nextInt();
            
            switch (opcao) {
                case 1:
                    vetorPreenchido = true;
                    System.out.println("-------------------------------------------------------------------------");
                    System.out.println("                       Vetor preenchido com SUCESSO!!");
                    System.out.println("-------------------------------------------------------------------------");
                    break;
                case 2:
                    if (vetorPreenchido) {
                        for (int i = 0; i < 10; i++) {
                            for (int j = 0; j < 10; j++) {
                                if (vector[i] < vector[j]) {
                                    auxOrdenacao = vector[i];
                                    vector[i] = vector[j];
                                    vector[j] = auxOrdenacao;
                                }
                            }
                        }
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("                        Vetor ordenado com SUCESSO!!");
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                                Vetor não preenchido!!");
                        System.out.println("      Por favor escolha a opção 1 antes de tentar ORDENAR o Vetor!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    break;
                case 3:
                    if (vetorPreenchido) {
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.print("Vetor: ");
                        for (int i = 0; i < 10; i++) {
                            System.out.print("[" + vector[i] + "]");
                        }
                        System.out.println();
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                         Vetor não preenchido!!");
                        System.out.println("     Por favor escolha a opção 1 antes de tentar imprimir o Vetor!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    break;
                case 4:
                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 5; j++) {
                            tabela[i][j] = aleatorio.nextInt(100);
                            vetorAuxDeOrdDaMatriz[k] = tabela[i][j];
                            k++;
                        }
                    }
                    k=0;
                    matrizPreenchida = true;
                    System.out.println("-------------------------------------------------------------------------");
                    System.out.println("                       Matriz preenchida com SUCESSO!!");
                    System.out.println("-------------------------------------------------------------------------");
                    break;
                case 5:
                    if (matrizPreenchida) {
                        for (int i = 0; i < 20; i++) {
                            for (int j = 0; j < 20; j++) {
                                if (vetorAuxDeOrdDaMatriz[i] < vetorAuxDeOrdDaMatriz[j]) {
                                    auxOrdenacao = vetorAuxDeOrdDaMatriz[i];
                                    vetorAuxDeOrdDaMatriz[i] = vetorAuxDeOrdDaMatriz[j];
                                    vetorAuxDeOrdDaMatriz[j] = auxOrdenacao;
                                }
                            }
                        }
                        k = 0;
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                tabela[i][j] = vetorAuxDeOrdDaMatriz[k];
                                k++;
                            }
                        }
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("                          Matriz ordenada com SUCESSO!!");
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                      Matriz não preenchida!!");
                        System.out.println("    Por favor escolha a opção 4 antes de tentar ORDENAR a Matriz!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    break;
                case 6:
                    if (matrizPreenchida) {
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("MATRIZ: ");
                        System.out.println();
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                System.out.print("[" + tabela[i][j] + "]");
                            }
                            System.out.println();
                        }
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                          Matriz não preenchida!!");
                        System.out.println("     Por favor escolha a opção 4 antes de tentar ORDENAR a Matriz!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    break;                    
                default:
                    System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    System.out.println("                       Opção Inválida! Tente novamente!");
                    System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
            }
        }
        for (int i = 0; i < 100; ++i) {
            System.out.println();
        }
        System.out.println("---------------------------------FIM!!!----------------------------------");
        System.out.println("kkkkkkkkkkkkkkkk Obrigado por usar nosso software kkkkkkkkkkkkkkkkkkkkkkk");
        System.out.println("-------------------------------------------------------------------------");
    }
}