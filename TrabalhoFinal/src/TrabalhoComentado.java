import java.util.Random;
import java.util.Scanner;

public class TrabalhoComentado {

    public static void main(String[] args) {
        //Objeto que será responsável por receber valores do usuário
        Scanner s = new Scanner(System.in);
        //Objeto que será responsável por gerar número aleatórios
        Random aleatorio = new Random();
        //Variável para uso no menu
        int opcao = 0;
        //Variáveis para uso na ordenação do vetor e da matriz
        int auxOrdenacao, k = 0;
        //Array usado no programa
        int[] vector = new int[10];
        //Array usado para auxiliar a ordenação da matriz
        int[] vetorAuxDeOrdDaMatriz = new int[20];
        //Matriz usada no programa
        int[][] tabela = new int[4][5];
        //Variáveis usada para retornar true quando a matriz e o array forem preenchidos
        boolean vetorPreenchido = false;
        boolean matrizPreenchida = false;

        //While que vai rodar enquanto o usuário o 
        //valor digitado n for o valor de saída do programa
        while (opcao != 7) {
            System.out.println("Menu:");
            System.out.println("1 - Preencher vetor");
            System.out.println("2 - Ordenar vetor");
            System.out.println("3 - Imprimir vetor");
            System.out.println("4 - Preencher matriz");
            System.out.println("5 - Ordenar matriz");
            System.out.println("6 - Imprimir matriz");
            System.out.println("7 - Sair");
            //Receber o valor digitado
            opcao = s.nextInt();

            if (!(opcao > 0 && opcao < 8)) {
                System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                System.out.println("                       Opção Inválida! Tente novamente!");
                System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                //Receber o valor digitado
                opcao = s.nextInt();
            }
            //switch usado executar uma tarefa de acordo com o valor da váriavel opcao
            switch (opcao) {
                //Caso opcao tenhao valor 1 ele vai executar seu conteúdo
                case 1:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    //Preencher o vetor
                    for (int i = 0; i < 10; i++) {
                        //O vetor vai receber valores aleatórios
                        vector[i] = aleatorio.nextInt(100);
                    }
                    //ao termino do preenchimento a variável preenchido vai receber valor true
                    vetorPreenchido = true;
                    System.out.println("-------------------------------------------------------------------------");
                    System.out.println("                       Vetor preenchido com SUCESSO!!");
                    System.out.println("-------------------------------------------------------------------------");
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
                //Caso opcao tenha o valor 2 ele vai executar seu conteúdo
                case 2:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }

                    //o if verifica se o valor de preenchido é true, se for,
                    //então que dizer que o vetor foi preenchido
                    if (vetorPreenchido) {
                        //aqui ele vai verificar todos os valores do array dez vezes, sempre colocando o menor pra esqueda
                        for (int i = 0; i < 10; i++) {
                            for (int j = 0; j < 10; j++) {
                                if (vector[i] < vector[j]) {
                                    auxOrdenacao = vector[i];
                                    vector[i] = vector[j];
                                    vector[j] = auxOrdenacao;
                                }
                            }
                        }
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("                        Vetor ordenado com SUCESSO!!");
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        //Se a condição retornar falsa o usuário vai se deparar com as mensagens abaixo
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                         Vetor não preenchido!!");
                        System.out.println("     Por favor escolha a opção 1 antes de tentar imprimir o Vetor!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
                //Caso opcao tenha o valor 3 ele vai executar seu conteúdo
                case 3:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }

                    //o if verifica se o valor de preenchido é true, se for,
                    //quer dizer que o vetor foi preenchido.
                    if (vetorPreenchido) {
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.print("Vetor: ");
                        //Se retorna verdadeiro, o for vai ser executado, imprimindo todos os elementos do array
                        for (int i = 0; i < 10; i++) {
                            System.out.print("[" + vector[i] + "]");
                        }
                        System.out.println();
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        //Se a condição retornar falsa o usuário vai se deparar com as mensagens abaixo
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                         Vetor não preenchido!!");
                        System.out.println("     Por favor escolha a opção 1 antes de tentar imprimir o Vetor!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
                //Caso opcao tenha o valor 3 ele vai executar seu conteúdo
                case 4:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }

                    //O for é executado 4 vez, verificando cada linha da matriz TABELA[][]
                    for (int i = 0; i < 4; i++) {
                        //Outro for vai ser executado, verificando cada coluna da linha atual
                        for (int j = 0; j < 5; j++) {
                            //Dentro desse for, cada elemento vai receber um numero aleatório entre 0 e 100
                            tabela[i][j] = aleatorio.nextInt(100);
                            //também um vetor vai receber todos os elementos da matriz, para auxiliar na ordenação da mesma
                            vetorAuxDeOrdDaMatriz[k] = tabela[i][j];
                            //A variável k, sugere os indices que vao receber o valor da matriz no array acima
                            k++;
                        }
                    }
                    //ao termino do preenchimento a variável preenchido vai receber valor true
                    matrizPreenchida = true;
                    System.out.println("-------------------------------------------------------------------------");
                    System.out.println("                       Matriz preenchida com SUCESSO!!");
                    System.out.println("-------------------------------------------------------------------------");
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
                //Caso opcao tenha o valor 3 ele vai executar seu conteúdo
                case 5:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }
                    //o if verifica se o valor de preenchido é true, se for,
                    //então que dizer que a matriz foi preenchida
                    if (matrizPreenchida) {
                        //Se retornar true, ofor abaixo é executado, ele vai ordenar os elemento do 
                        //vetorAuxDeOrdDaMatriz[] da msm forma que fez com anteriou, e usando a variável auxOrdenação
                        for (int i = 0; i < 20; i++) {
                            for (int j = 0; j < 20; j++) {
                                if (vetorAuxDeOrdDaMatriz[i] < vetorAuxDeOrdDaMatriz[j]) {
                                    auxOrdenacao = vetorAuxDeOrdDaMatriz[i];
                                    vetorAuxDeOrdDaMatriz[i] = vetorAuxDeOrdDaMatriz[j];
                                    vetorAuxDeOrdDaMatriz[j] = auxOrdenacao;
                                }
                            }
                        }
                        //o contador é zerado, para uso posterior
                        k = 0;
                        //O for abaixo pega os elementos já ordenados do vetorAuxDeOrdDaMatriz[]
                        //e os insere na matriz, obtendo assim uma matriz ordenada;
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                //A matriz tabela recebe o elemento do vetor, indicado pelo contado K,q equivale ao indice atual.
                                tabela[i][j] = vetorAuxDeOrdDaMatriz[k];
                                k++;
                            }
                        }
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("                          Matriz ordenada com SUCESSO!!");
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        //Se a condição retornar falsa o usuário vai se deparar com as mensagens abaixo
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                      Matriz não preenchida!!");
                        System.out.println("    Por favor escolha a opção 4 antes de tentar ORDENAR a Matriz!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
                //Caso opcao tenha o valor 3 ele vai executar seu conteúdo
                case 6:
                    //Gambiarra pra supor que limpei a tela!
                    for (int i = 0; i < 100; ++i) {
                        System.out.println();
                    }

                    if (matrizPreenchida) {
                        //Se retorna true o for abaixo é executado, imprimindo cada elemento da matiz,
                        //esteja ela ordenada ou não
                        System.out.println("-------------------------------------------------------------------------");
                        System.out.println("MATRIZ: ");
                        System.out.println();
                        for (int i = 0; i < 4; i++) {
                            for (int j = 0; j < 5; j++) {
                                System.out.print("[" + tabela[i][j] + "]");
                            }
                            System.out.println();
                        }
                        System.out.println("-------------------------------------------------------------------------");
                    } else {
                        //Se a condição retornar falsa o usuário vai se deparar com as mensagens abaixo
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                        System.out.println("                      Matriz não preenchida!!");
                        System.out.println("    Por favor escolha a opção 4 antes de tentar ORDENAR a Matriz!!!");
                        System.out.println("!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!-!");
                    }
                    //break para para a execução e passar proxima tarefa,
                    //que no caso é voltar o inicio, pro while
                    break;
            }
        }
        //Gambiarra pra supor que limpei a tela!
        for (int i = 0; i < 100; ++i) {
            System.out.println();
        }
        System.out.println("---------------------------------FIM!!!----------------------------------");
        System.out.println("kkkkkkkkkkkkkkkk Obrigado por usar nosso software kkkkkkkkkkkkkkkkkkkkkkk");
        System.out.println("-------------------------------------------------------------------------");
    }
}