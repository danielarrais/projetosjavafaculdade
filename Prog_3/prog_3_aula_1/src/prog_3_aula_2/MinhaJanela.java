package prog_3_aula_2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MinhaJanela extends JFrame {
	Color c = new Color(0x7B68EE);
	//Declara uma refer�ncia para um bot�o
	JButton bOk =  null;
	JPanel jPanel1 =  null;
	public MinhaJanela() {
		//Titulo da Janela
		setTitle("Minha primeira janela");
		//Tamanho da Janela
		setSize(1280,720);
		//Localiza��o inicial da Janela
		setLocation(0, 0);
		//Mudar cor do fundo da janela
		getContentPane().setBackground(c);
		//Altera opera��o de fechamento da janela
		//No caso, JFrame.EXIT_ON_CLOSE diz para o bot�o encerrar todo o sistema
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Permite ou n�o o redimensionamento
		setResizable(true);
		//Define a estrat�gia de layout a ser usada
		//setLayout(new FlowLayout());
		//Layout de grades
		//setLayout(new GridLayout(4, 10, 10, 10));
		//Layout de Borda
		setLayout(new BorderLayout(10,10));
		//Intancia o novo painel
		jPanel1 =  new JPanel();
		jPanel1.setLayout(new FlowLayout());
		jPanel1.add(new JButton("Button1"));
		jPanel1.add(new JButton("Button2"));
		jPanel1.add(new JButton("Button2"));
		
		//Int�ncia o bot�o
		bOk = new JButton("Confirmar");
		//Adiciona um componente visual � janela
//		for (int i=1; i<=40; i++) {
//			add(new JButton("Confirmar "+i));
//		}
		//O Segundo parem�tro s� � usado em layout do tipo Border
		add(jPanel1, BorderLayout.NORTH);
		add(new JButton("Leste"), BorderLayout.EAST);
		add(new JButton("Oeste"), BorderLayout.WEST);
		add(new JButton("Sul"), BorderLayout.SOUTH);
		add(new JButton("Centro"), BorderLayout.CENTER);
		//Torna todos os componentes vis�veis
		setVisible(true);
	}
}
