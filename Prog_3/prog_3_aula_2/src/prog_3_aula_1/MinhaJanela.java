package prog_3_aula_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;

public class MinhaJanela extends JFrame {
	JLabel meuRotolo = null;
	JTextField meuCampo = null;
	JButton bOk = null;
	JComboBox combo = null;
	String[] Linguagens = { "C#", "Java", "C++", "C" };
	JCheckBox termos = null;
	JRadioButton sexoM = null, sexoF = null;
	// Controla a sele��o do radios
	ButtonGroup grupo = null;
	JList lista = null;
	JTextArea texto = null;
	JScrollPane scroll = null;

	//Barra de menu
	JMenuBar menuBar = null;
	
	//Intens do menu
	JMenu arquivo = null;
	JMenu editar = null;
	JMenu cores = null;
	JMenu ajuda = null;
	
	//Subitens
	JMenuItem abrir = null;
	JMenuItem sair = null;
	JMenuItem vermelho = null;
	JMenuItem azul = null;
	JMenuItem sobre = null;
	
	JToolBar barrasFerramentas = null;
	JPanel painel = null;

	public MinhaJanela() {
		// Titulo da Janela
		setTitle("Minha primeira janela - Aula 2");
		// Tamanho da Janela
		setSize(1280, 720);
		// Localiza��o inicial da Janela
		// setLocation(0, 0);
		// Altera opera��o de fechamento da janela
		// No caso, JFrame.EXIT_ON_CLOSE diz para o bot�o encerrar todo o
		// sistema
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Permite ou n�o o redimensionamento
		setResizable(true);
		// Define a estrat�gia de layout a ser usada
		setLayout(new FlowLayout());
		meuRotolo = new JLabel("Digite seu Nome: ");
		meuCampo = new JTextField(20);
		meuCampo.setToolTipText("Digite seu nome");
		bOk = new JButton(new ImageIcon(getClass().getResource("confirm.png")));
		combo = new JComboBox(Linguagens);
		combo.setSelectedIndex(2);
		// Cria grupo de controle
		grupo = new ButtonGroup();
		sexoF = new JRadioButton("Feminino");
		sexoM = new JRadioButton("Masculino");
		// Adiciona os radius ao grupo
		grupo.add(sexoF);
		grupo.add(sexoM);

		lista = new JList<>(Linguagens);

		termos = new JCheckBox("Aceito!");

		texto = new JTextArea(10, 20);
		scroll = new JScrollPane(texto);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		arquivo = new JMenu("Arquivo");
		menuBar.add(arquivo);
		
		editar = new JMenu("Editar");
		menuBar.add(editar);
		cores = new JMenu("Cores");
		editar.add(cores);
		ajuda = new JMenu("Ajuda");
		menuBar.add(ajuda);
		abrir = new JMenuItem("Abrir");
		arquivo.add(abrir);
		arquivo.addSeparator();
		sair = new JMenuItem("Sair");
		arquivo.add(sair);
		
		azul = new JMenuItem("Azul");
		vermelho = new JMenuItem("Vermelho");
		sobre = new JMenuItem("Sobre");
		
		cores.add(azul);
		cores.add(vermelho);
		ajuda.add(sobre);
		
		painel = new JPanel();
		barrasFerramentas = new JToolBar();
		
		barrasFerramentas.add(new JButton("Opcao1"));
		barrasFerramentas.add(new JButton("Opcao2"));
		add(barrasFerramentas);
		
		painel.add(meuRotolo);
		painel.add(meuCampo);
		painel.add(bOk);
		painel.add(combo);
		painel.add(termos);
		painel.add(sexoF);
		painel.add(sexoM);
		painel.add(lista);
		painel.add(scroll);
		
		
		add(meuRotolo);
		add(meuCampo);
		add(bOk);
		add(combo);
		add(termos);
		add(sexoF);
		add(sexoM);
		add(lista);
		add(scroll);
		// Torna todos os componentes vis�veis
		setVisible(true);
	}
}
